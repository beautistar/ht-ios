//
//	ProductList.swift
//
//	Create by Dharmesh Avaiya on 15/12/2016
//	Copyright © 2016 Azilen Technologies. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ProductListModal : NSObject, NSCoding{

	var productId : String!
	var productImage : String!
	var productName : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		productId = dictionary["product_id"] as? String
		productImage = dictionary["product_image"] as? String
		productName = dictionary["product_name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
        
		if productId != nil{
			dictionary["product_id"] = productId
		}
		if productImage != nil{
			dictionary["product_image"] = productImage
		}
		if productName != nil{
			dictionary["product_name"] = productName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         productId = aDecoder.decodeObject(forKey: "product_id") as? String
         productImage = aDecoder.decodeObject(forKey: "product_image") as? String
         productName = aDecoder.decodeObject(forKey: "product_name") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {
        
		if productId != nil{
			aCoder.encode(productId, forKey: "product_id")
		}
		if productImage != nil{
			aCoder.encode(productImage, forKey: "product_image")
		}
		if productName != nil{
			aCoder.encode(productName, forKey: "product_name")
		}

	}

}
