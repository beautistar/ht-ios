//
//	UserList.swift
//
//	Create by Dharmesh Avaiya on 27/12/2016
//	Copyright © 2016 Azilen Technologies. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserList : NSObject, NSCoding{

	var isFollowed : String!
	var userid : String!
	var userimage : String!
	var username : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		isFollowed = dictionary["is_followed"] as? String
		userid = dictionary["userid"] as? String
		userimage = dictionary["userimage"] as? String
		username = dictionary["username"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if isFollowed != nil{
			dictionary["is_followed"] = isFollowed
		}
		if userid != nil{
			dictionary["userid"] = userid
		}
		if userimage != nil{
			dictionary["userimage"] = userimage
		}
		if username != nil{
			dictionary["username"] = username
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         isFollowed = aDecoder.decodeObject(forKey: "is_followed") as? String
         userid = aDecoder.decodeObject(forKey: "userid") as? String
         userimage = aDecoder.decodeObject(forKey: "userimage") as? String
         username = aDecoder.decodeObject(forKey: "username") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {

        if isFollowed != nil{
			aCoder.encode(isFollowed, forKey: "is_followed")
		}
		if userid != nil{
			aCoder.encode(userid, forKey: "userid")
		}
		if userimage != nil{
			aCoder.encode(userimage, forKey: "userimage")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}

	}

}
