//
//  RequestParamModal.swift
//  HT
//
//  Created by Dharmesh on 17/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import Foundation
import UIKit

class RequestParamModal : NSObject {
    
    var userId : String!
    var page : String!
    var action : String!
    var productId : String! //product_id
    var productSmallId : String! //product_id
    var productName : String! //product_name
    var productImage : [UIImage]! //product_image
    var productDescription : String! //product_description
    var productPrice : String! //product_price
    var category : String!
    var latitude : String!
    var longitude : String!
    var deviceId : String! //device_id
    var priceFrom : String! //price_from
    var priceTo : String! //price_to
    var searchtext : String! //searchtext
    var friendid : String!
    var userPhoto : String! //user_photo
    var username : String!
    var type : String!
    var email : String!
    var password : String!
    var phone : String!
    
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if userId != nil{
            dictionary["userid"] = userId
        }
        if page != nil{
            dictionary["page"] = page
        }
        if action != nil {
            dictionary["action"] = action
        }
        if productId != nil {
            dictionary["product_id"] = productId
        }
        if productSmallId != nil {
            dictionary["productid"] = productSmallId
        }
        if productName != nil {
            dictionary["product_name"] = productName
        }
        if productImage != nil {
            dictionary["product_image"] = productImage
        }
        if productDescription != nil {
            dictionary["product_description"] = productDescription
        }
        if productPrice != nil {
            dictionary["product_price"] = productPrice
        }
        if category != nil {
            dictionary["category"] = category
        }
        if latitude != nil {
            dictionary["latitude"] = latitude
        }
        if longitude != nil {
            dictionary["longitude"] = longitude
        }
        if deviceId != nil {
            dictionary["device_id"] = deviceId
        }
        if priceFrom != nil {
            dictionary["price_from"] = priceFrom
        }
        if priceTo != nil {
            dictionary["price_to"] = priceTo
        }
        if searchtext != nil {
            dictionary["searchtext"] = searchtext
        }
        if friendid != nil {
            dictionary["friendid"] = friendid
        }
        if userPhoto != nil {
            dictionary["user_photo"] = userPhoto
        }
        if username != nil {
            dictionary["username"] = username
        }
        if type != nil {
            dictionary["type"] = type
        }
        if email != nil {
            dictionary["email"] = email
        }
        if password != nil {
            dictionary["password"] = password
        }
        if phone != nil {
            dictionary["phone"] = phone
        }
        return dictionary
    }
}
