//
//	ProductDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ProductDetailModal : NSObject, NSCoding{
    
    var category : String!
    var countMutualFriend : Int!
    var isLiked : String!
    var isSold : String!
    var productDescription : String!
    var productId : String!
    var productImage : [String]!
    var productLatitude : String!
    var productLongitude : String!
    var productName : String!
    var productOwnerId : String!
    var productOwnerImage : String!
    var productOwnerName : String!
    var productOwnerPhone : String!
    var productPrice : String!
    var verifyFacebook : String!
    var verifyGoogle : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        category = dictionary["category"] as? String
        countMutualFriend = dictionary["count_mutual_friend"] as? Int
        isLiked = dictionary["is_liked"] as? String
        isSold = dictionary["is_sold"] as? String
        productDescription = dictionary["product_description"] as? String
        productId = dictionary["product_id"] as? String
        productImage = dictionary["product_image"] as? [String]
        productLatitude = dictionary["product_latitude"] as? String
        productLongitude = dictionary["product_longitude"] as? String
        productName = dictionary["product_name"] as? String
        productOwnerId = dictionary["product_owner_id"] as? String
        productOwnerImage = dictionary["product_owner_image"] as? String
        productOwnerName = dictionary["product_owner_name"] as? String
        productOwnerPhone = dictionary["product_owner_phone"] as? String
        productPrice = dictionary["product_price"] as? String
        verifyFacebook = dictionary["verify_facebook"] as? String
        verifyGoogle = dictionary["verify_google"] as? String
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if category != nil{
            dictionary["category"] = category
        }
        if countMutualFriend != nil{
            dictionary["count_mutual_friend"] = countMutualFriend
        }
        if isLiked != nil{
            dictionary["is_liked"] = isLiked
        }
        if isSold != nil{
            dictionary["is_sold"] = isSold
        }
        if productDescription != nil{
            dictionary["product_description"] = productDescription
        }
        if productId != nil{
            dictionary["product_id"] = productId
        }
        if productImage != nil{
            dictionary["product_image"] = productImage
        }
        if productLatitude != nil{
            dictionary["product_latitude"] = productLatitude
        }
        if productLongitude != nil{
            dictionary["product_longitude"] = productLongitude
        }
        if productName != nil{
            dictionary["product_name"] = productName
        }
        if productOwnerId != nil{
            dictionary["product_owner_id"] = productOwnerId
        }
        if productOwnerImage != nil{
            dictionary["product_owner_image"] = productOwnerImage
        }
        if productOwnerName != nil{
            dictionary["product_owner_name"] = productOwnerName
        }
        if productOwnerPhone != nil{
            dictionary["product_owner_phone"] = productOwnerPhone
        }
        if productPrice != nil{
            dictionary["product_price"] = productPrice
        }
        if verifyFacebook != nil{
            dictionary["verify_facebook"] = verifyFacebook
        }
        if verifyGoogle != nil{
            dictionary["verify_google"] = verifyGoogle
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        category = aDecoder.decodeObject(forKey:
            "category") as? String
        countMutualFriend = aDecoder.decodeObject(forKey: "count_mutual_friend") as? Int
        isLiked = aDecoder.decodeObject(forKey: "is_liked") as? String
        isSold = aDecoder.decodeObject(forKey: "is_sold") as? String
        productDescription = aDecoder.decodeObject(forKey: "product_description") as? String
        productId = aDecoder.decodeObject(forKey: "product_id") as? String
        productImage = aDecoder.decodeObject(forKey: "product_image") as? [String]
        productLatitude = aDecoder.decodeObject(forKey: "product_latitude") as? String
        productLongitude = aDecoder.decodeObject(forKey: "product_longitude") as? String
        productName = aDecoder.decodeObject(forKey: "product_name") as? String
        productOwnerId = aDecoder.decodeObject(forKey: "product_owner_id") as? String
        productOwnerImage = aDecoder.decodeObject(forKey: "product_owner_image") as? String
        productOwnerName = aDecoder.decodeObject(forKey: "product_owner_name") as? String
        productOwnerPhone = aDecoder.decodeObject(forKey: "product_owner_phone") as? String
        productPrice = aDecoder.decodeObject(forKey: "product_price") as? String
        verifyFacebook = aDecoder.decodeObject(forKey: "verify_facebook") as? String
        verifyGoogle = aDecoder.decodeObject(forKey: "verify_google") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder) {

        if category != nil{
            aCoder.encode(category, forKey: "category")
        }
        if countMutualFriend != nil{
            aCoder.encode(countMutualFriend, forKey: "count_mutual_friend")
        }
        if isLiked != nil{
            aCoder.encode(isLiked, forKey: "is_liked")
        }
        if isSold != nil{
            aCoder.encode(isSold, forKey: "is_sold")
        }
        if productDescription != nil{
            aCoder.encode(productDescription, forKey: "product_description")
        }
        if productId != nil{
            aCoder.encode(productId, forKey: "product_id")
        }
        if productImage != nil{
            aCoder.encode(productImage, forKey: "product_image")
        }
        if productLatitude != nil{
            aCoder.encode(productLatitude, forKey: "product_latitude")
        }
        if productLongitude != nil{
            aCoder.encode(productLongitude, forKey: "product_longitude")
        }
        if productName != nil{
            aCoder.encode(productName, forKey: "product_name")
        }
        if productOwnerId != nil{
            aCoder.encode(productOwnerId, forKey: "product_owner_id")
        }
        if productOwnerImage != nil{
            aCoder.encode(productOwnerImage, forKey: "product_owner_image")
        }
        if productOwnerName != nil{
            aCoder.encode(productOwnerName, forKey: "product_owner_name")
        }
        if productOwnerPhone != nil{
            aCoder.encode(productOwnerPhone, forKey: "product_owner_phone")
        }
        if productPrice != nil{
            aCoder.encode(productPrice, forKey: "product_price")
        }
        if verifyFacebook != nil{
            aCoder.encode(verifyFacebook, forKey: "verify_facebook")
        }
        if verifyGoogle != nil{
            aCoder.encode(verifyGoogle, forKey: "verify_google")
        }
        
    }
    
}
