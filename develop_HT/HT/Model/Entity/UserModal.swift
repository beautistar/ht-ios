//
//  UserModal.swift
//  HT
//
//  Created by Dharmesh on 19/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import Foundation
import UIKit

class UserModal  : NSObject, NSCoding {
    
    var userid : String!
    var userimage : String!
    var username : String!
    var aboutYou : String!
    var imgProfile : UIImage!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        userid = dictionary["userid"] as? String
        userimage = dictionary["userimage"] as? String
        username = dictionary["username"] as? String
        aboutYou = dictionary["about_you"] as? String
        imgProfile = dictionary["image"] as? UIImage
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if userid != nil{
            dictionary["userid"] = userid
        }
        if userimage != nil{
            dictionary["userimage"] = userimage
        }
        if username != nil{
            dictionary["username"] = username
        }
        if aboutYou != nil{
            dictionary["about_you"] = aboutYou
        }
        if imgProfile != nil{
            dictionary["image"] = imgProfile
        }

        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder) {
        
        userid = aDecoder.decodeObject(forKey: "userid") as? String
        userimage = aDecoder.decodeObject(forKey: "userimage") as? String
        username = aDecoder.decodeObject(forKey: "username") as? String
        aboutYou = aDecoder.decodeObject(forKey: "about_you") as? String
        imgProfile = aDecoder.decodeObject(forKey: "image") as? UIImage
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder) {

        if userid != nil{
            aCoder.encode(userid, forKey: "userid")
        }
        if userimage != nil{
            aCoder.encode(userimage, forKey: "userimage")
        }
        if username != nil{
            aCoder.encode(username, forKey: "username")
        }
        if aboutYou != nil{
            aCoder.encode(aboutYou, forKey: "about_you")
        }
        if imgProfile != nil{
            aCoder.encode(imgProfile, forKey: "image")
        }
    }
}
