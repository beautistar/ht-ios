//
//	MessageModal.swift
//
//	Create by Dharmesh Avaiya on 15/12/2016
//	Copyright © 2016 Azilen Technologies. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class MessageModal : NSObject, NSCoding{

	var productList : [ProductListModal]!
    var productDetail : ProductDetailModal!
    var userInfo : UserModal!
    var userProfile : UserProfile!
    var userList : [UserList]!
    
    var id : Int!
	var resultCode : Int!
    var error : String!
    
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary) {		
        
        productList = [ProductListModal]()
		
        resultCode = dictionary["result_code"] as? Int
        id = dictionary["id"] as? Int
        error = dictionary["error"] as? String
        if let productListArray = dictionary["product_list"] as? [NSDictionary]{
			for dic in productListArray{
				let value = ProductListModal(fromDictionary: dic)
				productList.append(value)
			}
		}
        if let productDetailData = dictionary["product_detail"] as? NSDictionary{
            productDetail = ProductDetailModal(fromDictionary: productDetailData)
        }
        if let userInfoData = dictionary["user_info"] as? NSDictionary{
            userInfo = UserModal(fromDictionary: userInfoData)
        }
        if let userProfileData = dictionary["user_profile"] as? NSDictionary{
            userProfile = UserProfile(fromDictionary: userProfileData)
        }
        
        userList = [UserList]()
        if let userListArray = dictionary["user_list"] as? [NSDictionary]{
            for dic in userListArray{
                let value = UserList(fromDictionary: dic)
                userList.append(value)
            }
        }
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		
        if resultCode != nil{
            dictionary["result_code"] = resultCode
        }
        if id != nil{
            dictionary["id"] = id
        }
        if error != nil {
            dictionary["error"] = error
        }
        if productList != nil {
			var dictionaryElements = [NSDictionary]()
			for productListElement in productList {
				dictionaryElements.append(productListElement.toDictionary())
			}
			dictionary["product_list"] = dictionaryElements
		}
        if productDetail != nil{
            dictionary["product_detail"] = productDetail.toDictionary()
        }
        
        if userInfo != nil{
            dictionary["user_info"] = userInfo.toDictionary()
        }
        if userProfile != nil{
            dictionary["user_profile"] = userProfile.toDictionary()
        }
        if userList != nil{
            var dictionaryElements = [NSDictionary]()
            for userListElement in userList {
                dictionaryElements.append(userListElement.toDictionary())
            }
            dictionary["user_list"] = dictionaryElements
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         productList = aDecoder.decodeObject(forKey: "product_list") as? [ProductListModal]
         productDetail = aDecoder.decodeObject(forKey: "product_detail") as? ProductDetailModal
         resultCode = aDecoder.decodeObject(forKey: "result_code") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? Int
         userInfo = aDecoder.decodeObject(forKey: "user_info") as? UserModal
         userProfile = aDecoder.decodeObject(forKey: "user_profile") as? UserProfile
         userList = aDecoder.decodeObject(forKey: "user_list") as? [UserList]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {
        
		if productList != nil{
			aCoder.encode(productList, forKey: "product_list")
		}
        if productDetail != nil{
            aCoder.encode(productDetail, forKey: "product_detail")
        }
		if resultCode != nil{
			aCoder.encode(resultCode, forKey: "result_code")
		}
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if userInfo != nil{
            aCoder.encode(userInfo, forKey: "user_info")
        }
        if userProfile != nil{
            aCoder.encode(userProfile, forKey: "user_profile")
        }
        if userList != nil{
            aCoder.encode(userList, forKey: "user_list")
        }
	}
}
