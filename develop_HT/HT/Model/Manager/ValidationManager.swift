//
//  ValidationManager.swift
//  HT
//
//  Created by Dharmesh on 16/01/17.
//  Copyright © 2017 dharmesh. All rights reserved.
//

import Foundation

class ValidationManager: NSObject {
    
    static var singleton = ValidationManager()
    
    func isValidEmail(email : String) -> Bool {       
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email.lowercased())
    }
    
    func isValidPassword(password : String) -> Bool {
        if password.characters.count >= 4 {
            return true
        } else {
            return false
        }
    }
    
    func isValidFullName(string : String) -> Bool {
        
        if string.characters.count <= 18 {
            
            let spaceCount = string.characters.filter{$0 == " "}.count
            if spaceCount <= 1 {
                
                let RegEx = "^[a-zA-Z][a-zA-Z\\s]+$"
                let fullNameTest = NSPredicate(format:"SELF MATCHES %@", RegEx)
                return fullNameTest.evaluate(with: string)
            } else {
                return false
            }
            
        } else {
            return false
        }
    }
    
    func isValidPhone(phone : String) -> Bool {
        
        let PHONE_REGEX = "(?:(\\+\\d\\d\\s+)?((?:\\(\\d\\d\\)|\\d\\d)\\s+)?)(\\d{4,5}\\-?\\d{4})"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: phone)
        return result
    }
}
