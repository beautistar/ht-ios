//
//  AppDelegate.swift
//  HT
//
//  Created by Dharmesh on 30/11/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AFNetworking

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInUIDelegate, GIDSignInDelegate {

    var window: UIWindow?
    var linkType = 1
    
    static var singleton : AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    //------------------------------------------------------
    
    //MARK: AppDelegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //start monitoring network connectivity
        AFNetworkReachabilityManager.shared().startMonitoring()
        AFNetworkActivityIndicatorManager.shared().isEnabled = true
        
        IQKeyboardManager.sharedManager().enable = true
        
        if userId == 0 {
            NavigationManager.singleton.setupLogin()
        } else {
            NavigationManager.singleton.setupHome()
        }
            
        /*let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()*/
        application.applicationIconBadgeNumber = 0
        
        // Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        GIDSignIn.sharedInstance().delegate = self
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        FBSDKAppEvents.activateApp()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    //---------------------------------------------------------------
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
       
        if linkType == 1 {
            
            if(FBSDKAccessToken.current() != nil) {
                
                return FBSDKApplicationDelegate.sharedInstance().application(
                    application,
                    open: url,
                    sourceApplication: sourceApplication,
                    annotation: annotation)
            } else {
               
                if(MyUserDefaults.bool(forKey: PREF_FIRST_FB_SIGNUP)) {
                    return FBSDKApplicationDelegate.sharedInstance().application(
                        application,
                        open: url,
                        sourceApplication: sourceApplication,
                        annotation: annotation)
                } else {
                    return FBSDKApplicationDelegate.sharedInstance().application(
                        application,
                        open: url,
                        sourceApplication: sourceApplication,
                        annotation: annotation)
                }
            }
        } else if linkType == 2 {
            return GIDSignIn.sharedInstance().handle(url,
                                                        sourceApplication: sourceApplication,
                                                        annotation: annotation)
        } else {
            return true
        }
    }
    
    //------------------------------------------------------
    
    //MARK: GIDSignInDelegate
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
     
        if (error == nil) {
            
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
            
            print(userId ?? "")
            print(idToken ?? "")
            print(fullName ?? "")
            print(givenName ?? "")
            print(familyName ?? "")
            print(email ?? "")
            
            MyNotificationCenter.post(name: NSNotification.Name(rawValue: kNotificationVerifyLink), object: nil)
            
            /*let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
             let viewController = mainStoryboard.instantiateViewControllerWithIdentifier("tabBarcontroller") as! UITabBarController
             UIApplication.sharedApplication().keyWindow?.rootViewController = viewController;*/
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print(user)
    }
    
    //------------------------------------------------------

}

