//
//  NavigationManager.swift
//  HT
//
//  Created by Dharmesh on 03/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation

enum NavigationScreenType {
    
    case NavigationScreenTypeMenu
    case NavigationScreenTypeHome
    case NavigationScreenTypePost
    case NavigationScreenTypeMyNetwords
    case NavigationScreenTypeHelp
    case NavigationScreenTypeInviteFriends
    case NavigationScreenTypeSettings
    case NavigationScreenTypeSearch
    case NavigationScreenTypeFollowing
    case NavigationScreenTypeFollowers
    case NavigationScreenTypeFindFriends
    case NavigationScreenTypeFindFriendsDetail
    case NavigationScreenTypeProfile
    case NavigationScreenTypeListed
    case NavigationScreenTypeLikes
    case NavigationScreenTypeBought
    case NavigationScreenTypeHelpDetailForm
    case NavigationScreenTypeEditProfile
    case NavigationScreenTypeProfileDetail
    case NavigationScreenTypeMap
    case NavigationScreenTypeLogin
    case NavigationScreenTypeSignup
    case NavigationScreenTypeImageSlider
}

enum NavigationType {
    
    case NavigationTypeHome
    case NavigationTypePost
    case NavigationTypeMyNetwords
    case NavigationTypeHelp
    case NavigationTypeInviteFriends
    case NavigationTypeSettings
    case NavigationTypeLogin
}

class NavigationManager : NSObject {
    
    static let singleton : NavigationManager = NavigationManager()
    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    
    //MARK: Memory Management Method
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Setup Roots
    
    func setupLogin() {
        
        let loginVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeLogin)
        let window = AppDelegate.singleton.window
        window?.backgroundColor = kColorBackground
        window?.tintColor = kColorRed
        window?.rootViewController = loginVC
        window?.makeKeyAndVisible()
    }
    
    func setupHome() {
        
        let window = AppDelegate.singleton.window
        
        let leftViewController = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeMenu)
        let navigationController = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeHome)
        
        let slideMenuController = ExSlideMenuController(mainViewController:navigationController, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        //slideMenuController.delegate = mainViewController
        window?.backgroundColor = kColorBackground
        window?.tintColor = kColorRed
        window?.rootViewController = slideMenuController
        window?.makeKeyAndVisible()
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func navigation(screenType : NavigationType) -> UINavigationController {
                      
        switch screenType {
            
        case .NavigationTypeHome:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "NavigationHomeVC") as! UINavigationController
            controller.navigationBar.barTintColor = kColorOrange
            return controller
        case .NavigationTypePost:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "navigationPostVC") as! UINavigationController
            controller.navigationBar.barTintColor = kColorOrange
            return controller
        case .NavigationTypeMyNetwords:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "navigationMyNetworksVC") as! UINavigationController
            controller.navigationBar.barTintColor = kColorOrange
            return controller
        case .NavigationTypeHelp:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "navigationHelpVC") as! UINavigationController
            controller.navigationBar.barTintColor = kColorOrange
            return controller
        case .NavigationTypeInviteFriends:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "navigationInviteFriendsVC") as! UINavigationController
            controller.navigationBar.barTintColor = kColorOrange
            return controller
        case .NavigationTypeSettings:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "navigationSettingsVC") as! UINavigationController
            controller.navigationBar.barTintColor = kColorOrange
            return controller
        case .NavigationTypeLogin :
            let controller = mainStoryboard.instantiateViewController(withIdentifier: "navigationLoginVC") as! UINavigationController
            controller.navigationBar.barTintColor = kColorOrange
            return controller
        }
    }
    
    func screen(screenType : NavigationScreenType) -> UIViewController {
        
        switch screenType {
            
        case .NavigationScreenTypeMenu:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(LeftViewController.self)) as! LeftViewController
            return controller
            
        case .NavigationScreenTypeHome:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(MainViewController.self)) as! MainViewController
            return controller
            
        case .NavigationScreenTypePost:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(PostVC.self)) as! PostVC
            return controller
            
        case .NavigationScreenTypeMyNetwords:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(MyNetworksVC.self)) as! MyNetworksVC
            return controller
            
        case .NavigationScreenTypeHelp:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(HelpVC.self)) as! HelpVC
            return controller
            
        case .NavigationScreenTypeInviteFriends:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(InviteFriendsVC.self)) as! InviteFriendsVC
            return controller
            
        case .NavigationScreenTypeSettings:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(SettingsVC.self)) as! SettingsVC
            return controller
            
        case .NavigationScreenTypeSearch:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(SearchVC.self)) as! SearchVC
            return controller
            
        case .NavigationScreenTypeFollowing:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(FollowingVC.self)) as! FollowingVC
            return controller
            
        case .NavigationScreenTypeFollowers:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(FollowersVC.self)) as! FollowersVC
            return controller
            
        case .NavigationScreenTypeFindFriends:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(FindFriendsVC.self)) as! FindFriendsVC
            return controller
        case .NavigationScreenTypeFindFriendsDetail:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(FindFriendDetailsVC.self)) as! FindFriendDetailsVC
            return controller
        case .NavigationScreenTypeProfile:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(ProfileVC.self)) as! ProfileVC
            return controller
        case .NavigationScreenTypeListed:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(ListedVC.self)) as! ListedVC
            return controller
        case .NavigationScreenTypeLikes:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(LikedVC.self)) as! LikedVC
            return controller
        case .NavigationScreenTypeBought:
            
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(BoughtVC.self)) as! BoughtVC
            return controller
        case .NavigationScreenTypeHelpDetailForm:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(HelpDetailFormVC.self)) as! HelpDetailFormVC
            return controller
        case .NavigationScreenTypeEditProfile:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(EditProfileVC.self)) as! EditProfileVC
            return controller
        case .NavigationScreenTypeProfileDetail:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(ProfileDetailVC.self)) as! ProfileDetailVC
            return controller
        case .NavigationScreenTypeMap:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(MapVC.self)) as! MapVC
            return controller
        case .NavigationScreenTypeLogin:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(LoginVC.self)) as! LoginVC
            return controller
        case .NavigationScreenTypeSignup:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(SignupVC.self)) as! SignupVC
            return controller
        case .NavigationScreenTypeImageSlider:
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String.className(ImageSliderVC.self)) as! ImageSliderVC
            return controller
        }
    }
    
    //------------------------------------------------------
    
    func shareIt(view : UIViewController, message : String?, image : UIImage?) {
        
        //Set the default sharing message.
        let message = message ?? kAppName.localized()
        
        //Set the link to share.
        
        if let link = NSURL(string: "http://www.htayiti.com")
        {
            var objectsToShare = [message, link] as [Any]
            if image != nil {
                objectsToShare.append(image!)
            }
            
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            view.present(activityVC, animated: true, completion: nil)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Initialisation
    
    override init() {
        
    }
    
    //------------------------------------------------------
}
