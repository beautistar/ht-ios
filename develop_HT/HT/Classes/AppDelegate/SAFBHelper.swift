//
//  SAFBHelper.swift
//  HT
//
//  Created by Dharmesh on 30/11/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Social
import Accounts

@objc protocol SAFBHelperDelegate {
    
    func fbUserLoggedIn(dicParams:NSMutableDictionary)
    func fbUserFailedToLogIn(error:NSError)
    
}
class SAFBHelper: NSObject {
    
    var facebookAccount:ACAccount?
    var facebookAccountType:ACAccountType?
    var accountStore:ACAccountStore?
    var dicResponse:NSDictionary?
    var delegate: SAFBHelperDelegate?
    
    func loginToFacebook(){
        self.fbSDKLogin()
    }
    
    func getFBAccountDetails(fbAppID:String){
        
        if((accountStore) == nil){
            accountStore = ACAccountStore()
        }
        if(facebookAccountType == nil){
            facebookAccountType = accountStore?.accountType(withAccountTypeIdentifier: ACAccountTypeIdentifierFacebook)
        }
        
        
        let options = [ACFacebookAppIdKey:fbAppID, ACFacebookPermissionsKey: ["email"], ACFacebookAudienceKey: ACFacebookAudienceFriends] as [String : Any]
        accountStore?.requestAccessToAccounts(with: facebookAccountType, options: options, completion: { (granted : Bool, error : Error?) in
           
            if(granted){
                
                let accounts = self.accountStore?.accounts(with: self.facebookAccountType)
                self.facebookAccount = accounts?.last as? ACAccount
                //let facebookCredential:ACAccountCredential = (self.facebookAccount?.credential)!
                //var accessToken = facebookCredential.oauthToken
                //self.getFacebookData(fbAppID, facebookAccount: self.facebookAccount!)
                
            }
            else{
                
                self.dicResponse?.setValue(error, forKey: "error")
                print("error: \(error)")
                
            }
        })
    }
    /*
    func getFacebookData(fbAppID:String,facebookAccount:ACAccount){
        
        var requesrURL:NSURL = NSURL(string: "https://graph.facebook.com/me")!
        
        var request:SLRequest = SLRequest(forServiceType: SLServiceTypeFacebook, requestMethod: SLRequestMethod.GET, URL: requesrURL, parameters: nil)
        request.account = facebookAccount
        
        
        request.performRequestWithHandler({ (responseData: NSData!,urlResponse: NSHTTPURLResponse!,error: NSError!) -> Void in
            
            if(error == nil){
                
                var errorPointer: NSError?
                var responseDictionary :NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
                
                
                self.dicResponse?.setValue(responseDictionary, forKey: "dicResponse")
                
                if(responseDictionary.objectForKey("error") != nil){
                    self.attemptRenewFBCredentials(fbAppID)
                }
                
                
            }else{
                
                self.dicResponse?.setValue(error, forKey: "error")
                print("Error : \(error)")
                
            }
            
        })
        
        
        self.accountStore = ACAccountStore()
        var FBaccountType:ACAccountType? = self.accountStore?.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierFacebook)
        
        var array:NSArray = ["email"]
        var key = fbAppID
        
        
        var dicFB:NSDictionary = NSDictionary(objectsAndKeys: key as NSString,ACFacebookAppIdKey as NSString,array,array) as NSDictionary
        
        self.accountStore?.requestAccessToAccountsWithType(FBaccountType, options: dicFB as [NSObject : AnyObject], completion: { (granted :Bool,error: NSError!) -> Void in
            
        })
        
        
    }
    */
    //----------------------------------------------------------------------------------
    //Note:- this is not tested yet,Reason: No permission from FB to use another account
    //----------------------------------------------------------------------------------
    func attemptRenewFBCredentials(fbAppID:String){
        
        accountStore?.renewCredentials(for: self.facebookAccount, completion: { (renewResult : ACAccountCredentialRenewResult, error : Error?) in
            
            if(error == nil){
                switch renewResult{
                case ACAccountCredentialRenewResult.renewed:
                    self.getFBAccountDetails(fbAppID: fbAppID)
                case ACAccountCredentialRenewResult.rejected:
                    print("User decliend permission")
                case ACAccountCredentialRenewResult.failed:
                    print("Internal error try again")
                }
            } else {
                print("Error in attemptRenewFBCredentials: \(error)")
            }
        })
    }
    
    
    
    func checkForSession(){
        
        if(FBSDKAccessToken.current() != nil) {
            
            let permissions = ["public_profile", "email"]
            let loginManager:FBSDKLoginManager = FBSDKLoginManager()
            loginManager.logIn(withReadPermissions: permissions, handler: { (result, error) -> Void in
                
                if (result != nil) {
                    self.sessionStateChanged(permissions: permissions as NSArray, result: result!, error: nil)
                } else if (error != nil) {
                    self.sessionStateChanged(permissions: permissions as NSArray, result: result!, error: error! as NSError?)
                }
            })
        }
    }
    
    
    //----------------------------------------------------------------------------------
    //MARK: -- Handles session state changes in the app
    //----------------------------------------------------------------------------------
    
    func fbSDKLogin(){
        
        // If the session state is any of the two "open" states when the button is clicked
        
        if(FBSDKAccessToken.current() != nil){
            
            self.fetchFBBasicDetails()
            
            //FBSession.activeSession().closeAndClearTokenInformation()
            
            // If the session state is not any of the two "open" states when the button is clicked
        }else{
            
            // Open a session showing the user the login UI
            // You must ALWAYS ask for public_profile permissions when opening a session
            let permissions = ["public_profile", "email"]
            MyUserDefaults.set(true, forKey: PREF_FIRST_FB_SIGNUP)
            
            let loginManager:FBSDKLoginManager = FBSDKLoginManager()
           
            loginManager.logIn(withReadPermissions: permissions, handler: { (result, error) -> Void in
                
                if((error) != nil) {
                    print(error!)
                } else {
                    self.sessionStateChanged(permissions: permissions as NSArray, result: result!, error: error as NSError?)
                }
            })
            
            
        }
        
    }
    func sessionStateChanged(permissions:NSArray, result:FBSDKLoginManagerLoginResult, error : NSError?)
    {
        if (error != nil)
        {
            print("error \(error)")
        }
        else if result.isCancelled {
            
            let err : NSError = NSError(domain: "Cancelled", code: 008, userInfo: nil)
            
            self.delegate?.fbUserFailedToLogIn(error: err)
            
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email")
            {
                
                self.fetchFBBasicDetails()
            }
        }
    }
    
    func fetchFBBasicDetails(){
        
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,first_name,gender"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if(error == nil){
                
                let fbID : String = (result! as! NSDictionary).object(forKey: "id") as! String
                let accessToken : AnyObject = FBSDKAccessToken.current()
                let loginType = 1												//1 for FB,2 for Twitter
                
                var strFName = ""
                if let fName: String = (result! as! NSDictionary).object(forKey: "first_name") as? String {
                    strFName = fName
                }
                
                var strLName = ""
                if let lName:String = (result! as! NSDictionary).object(forKey: "last_name") as? String  {
                    strLName = lName
                }
                
                let deviceType = 1												//1 for iOS 2 for Android
               // var version:AnyObject = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString")!
                var strEmail = ""
                if let email:String = (result! as! NSDictionary).object(forKey: "email") as? String {
                    strEmail = email
                }
               // var fullName: AnyObject? = result.objectForKey("name")
                //var gender: AnyObject? = result.objectForKey("gender")
                let imageURL = NSString(format: "http://graph.facebook.com/%@/picture?type=large", fbID as NSString)
                let dicParams:NSMutableDictionary = ["facebook_id":fbID,"access_token":accessToken,"login_type":loginType,"first_name":strFName,"last_name":strLName,"device_type":deviceType,"user_pic":imageURL,]
                
                if(strEmail != "") { //Add email if user have
                    dicParams.setValue(strEmail, forKey: "email")
                }
                
                print("WS Login : dicParams\(dicParams)")
                self.delegate?.fbUserLoggedIn(dicParams: dicParams)
                
                
            }
            else{
                self.delegate?.fbUserFailedToLogIn(error: error as! NSError)
            }
        })
        
    }
    
//    class func postFbShare(viewController:UIViewController)
//    {
//        let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
//        
//        FBSDKShareDialog.showFromViewController(viewController, withContent: content, delegate: nil)
//    }
    
    class func logoutFromFB(){
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    //--------------------------------------------------------------------------------------------------
    //MARK: - End for SDK methods
    //--------------------------------------------------------------------------------------------------
    
    
}
