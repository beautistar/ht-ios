//
//  AppConstants.swift
//  HT
//
//  Created by Dharmesh on 30/11/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift
import MBProgressHUD

let kAppName = "HT".localized()

let kColorOrange = UIColor(hex: "f26e22")
let kColorBackground = UIColor(hex: "EEEEEE")
let kColorRed = UIColor(hex: "DB4A54")
let kColorLike = UIColor(hex: "EB505A")
let kColorGray = UIColor.gray

var MyNotificationCenter : NotificationCenter {
    return NotificationCenter.default
}

let kNotificationHomeVC = "NotificationHomeVC"
let kNotificationPostVC = "NotificationPostVC"
let kNotificationMyNetworkVC = "NotificationMyNetworkVC"
let kNotificationHelpVC = "NotificationHelpVC"
let kNotificationSettingsVC = "NotificationSettingsVC"
let kNotificationVerifyLink = "NotificationVerifyLink"

var BlankView : UIView {
    
    let blankView = UIView()
    blankView.backgroundColor = UIColor.clear
    return blankView
}

var ScreenRect : CGRect {
    return UIScreen.main.bounds
}

var MyUserDefaults : UserDefaults {
    return UserDefaults.standard
}

let kCurrentUser = "currentUser"
let kCurrentUserId = "currentUserId"

var userData : UserModal? {
    if let userData = MyUserDefaults.object(forKey: kCurrentUser) as? NSData {
        let dict = NSKeyedUnarchiver.unarchiveObject(with: userData as Data) as! NSDictionary
        return UserModal(fromDictionary: dict)
    }
    return nil
}

var userId : Int {
    
    let userId = MyUserDefaults.object(forKey: kCurrentUserId) as? Int
    if userId == nil {
        return 0
    } else {
        return userId!
    }
}

/*func Localized(key : String) ->  String {
    return NSLocalizedString(key, comment: String())
}*/

let kCancel = "Cancel".localized()
let kOk = "Ok".localized()
let kCamera = "Camera".localized()
let kGallery = "Gallery".localized()
let kSettings = "Settings".localized()

//social 
let PREF_FIRST_FB_SIGNUP = "Pref_first_fb_signup"

func LocalizedControl(view : Any) {
    
    if view is UIButton {
        
        let button = view as! UIButton
        let keyNormal = button.title(for: .normal)
        let keyHighlighted = button.title(for: .highlighted)
        let keySelected = button.title(for: .selected)
        let keyDisable = button.title(for: .disabled)        
        
        /*if keyNormal != nil {
            button .setTitle(Localized(key: keyNormal!), for: .normal)
        }
        
        if keyHighlighted != nil {
            button .setTitle(Localized(key: keyNormal!), for: .highlighted)
        }
        
        if keySelected != nil {
            button .setTitle(Localized(key: keyNormal!), for: .selected)
        }
        
        if keyDisable != nil {
            button .setTitle(Localized(key: keyNormal!), for: .disabled)
        }*/
        
        if keyNormal != nil {
            button .setTitle(keyNormal!.localized(), for: .normal)
        }
        
        if keyHighlighted != nil {
            button .setTitle(keyNormal!.localized(), for: .highlighted)
        }
        
        if keySelected != nil {
            button .setTitle(keyNormal!.localized(), for: .selected)
        }
        
        if keyDisable != nil {
            button .setTitle(keyNormal!.localized(), for: .disabled)
        }
    }
    
    if view is UILabel {
        
        let label = view as! UILabel
        let key = label.text
        
        if key != nil {
            //label.text = Localized(key: key!)
            label.text = key!.localized()
        }
    }
    
    if view is UITextField {
        
        let label = view as! UITextField
        let key = label.placeholder
        
        if key != nil {
            //label.placeholder = Localized(key: key!)
            label.placeholder = key!.localized()
        }
    }
}

func LocalizedControl(view : Any, key : String) {
    
    if view is UIButton {
        
        let button = view as! UIButton
        
        button .setTitle(key.localized(), for: .normal)
        button .setTitle(key.localized(), for: .highlighted)
        button .setTitle(key.localized(), for: .selected)
        button .setTitle(key.localized(), for: .disabled)
    }
    
    if view is UILabel {
        
        let label = view as! UILabel
        label.text = key.localized()
    }
    
    if view is UITextField {
        
        let label = view as! UITextField
        label.placeholder = key.localized()
    }
}

func messageBox(message : String!) {
    let av = UIAlertView(title: kAppName, message: message.localized(), delegate: nil, cancelButtonTitle: kOk)
    av.show()
}

func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
    
    let contextImage: UIImage = UIImage(cgImage: image.cgImage!)
    
    let contextSize: CGSize = contextImage.size
    
    var posX: CGFloat = 0.0
    var posY: CGFloat = 0.0
    var cgwidth: CGFloat = CGFloat(width)
    var cgheight: CGFloat = CGFloat(height)
    
    // See what size is longer and create the center off of that
    if contextSize.width > contextSize.height {
        posX = ((contextSize.width - contextSize.height) / 2)
        posY = 0
        cgwidth = contextSize.height
        cgheight = contextSize.height
    } else {
        posX = 0
        posY = ((contextSize.height - contextSize.width) / 2)
        cgwidth = contextSize.width
        cgheight = contextSize.width
    }
    
    let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
    
    // Create bitmap image from context using the rect
    let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
    
    // Create a new image based on the imageRef and rotate back to the original orientation
    let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
    
    return image
}

func progressHUD(view : UIView) -> MBProgressHUD {
    
    let hud = MBProgressHUD .showAdded(to:view, animated: true)
    hud.mode = .annularDeterminate
    hud.label.text = "Loading";
    hud.animationType = .zoomIn
    hud.tintColor = UIColor.white
    hud.contentColor = kColorOrange
    return hud
}
