//
//  FindFriendDetailsVC.swift
//  HT
//
//  Created by Dharmesh on 04/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Toucan

class FindFriendDetailsVC : UIViewController {
    
    @IBOutlet weak var tblFollowing: UITableView!
    @IBOutlet weak var searchTextBar: UISearchBar!
    
    var followingController : FollowingVC!
    var followersController : FollowersVC!
    var findFriendController : FindFriendsVC!
    
    var userList : [UserList] = []
    var userListBackup : [UserList] = []
    
    let imgCheckBoxSelected = #imageLiteral(resourceName: "ic_following_list")
    let imgCheckBoxBlank = #imageLiteral(resourceName: "ic_add_following")
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {

        searchTextBar.delegate = self
        self.title = "Find Friends".localized()
        tblFollowing.separatorStyle = .none
    }
    
    func requestForSearchFriend(searchText : String, isFromSearch : Bool) {
        
        let request = "\(kAPISearchFriend)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPISearchFriend
        requestParam.userId = String(userId)
        requestParam.searchtext = searchText
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: false, successBlock: { (resposne :MessageModal, message : String?) in

            if self.searchTextBar.text?.characters.count == 0 {
                self.userList.removeAll()
            } else {
                
                self.userList = resposne.userList
                if self.userList.count >= 0 {
                    self.tblFollowing.separatorStyle = .singleLine
                }
                if !isFromSearch {
                    self.userListBackup = resposne.userList
                }
            }
            self.tblFollowing.reloadData()
            
        }, failureBlock: {(error : Error?) in
            
            self.userList.removeAll()
            self.tblFollowing.reloadData()
        })
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        //requestForSearchFriend(searchText: "", isFromSearch: false)
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}

extension FindFriendDetailsVC : UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.endEditing(true)
        self.requestForSearchFriend(searchText: searchBar.text!, isFromSearch: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.endEditing(true)
        searchBar.text = ""
        userList = []
        tblFollowing.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        print(searchText)
        
        if searchText == "" {
            tblFollowing.separatorStyle = .none
            userList.removeAll()
            tblFollowing.reloadData()
        }
        else {
            
            let debouncedFunction = Debouncer(delay: 0.40) {
                self.requestForSearchFriend(searchText: searchBar.text!, isFromSearch: true)
            }
            debouncedFunction.call()
        }
    }
}

extension FindFriendDetailsVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let user = userList[indexPath.row]
        if (user.isFollowed == "Y") {
            cell.accessoryView = UIImageView(image: imgCheckBoxSelected)
        } else {
            cell.accessoryView = UIImageView(image: imgCheckBoxBlank)
        }
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let friendUser = userList[indexPath.row]
        
        if friendUser.isFollowed == "N" {
            
            let request = "\(kAPIFollowFriend)"
            
            let requestParam = RequestParamModal()
            requestParam.action = kAPIFollowFriend
            requestParam.userId = String(userId)
            requestParam.friendid = friendUser.userid
            
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                friendUser.isFollowed = "Y"
                let cell = tableView.cellForRow(at: indexPath) as! FollowingCell
                cell.accessoryView = UIImageView(image: self.imgCheckBoxSelected)
                
            }, failureBlock: {(error : Error?) in
                
            })
            
        } else {
            
            let request = "\(kAPIUnfollowFriend)"
            
            let requestParam = RequestParamModal()
            requestParam.action = kAPIUnfollowFriend
            requestParam.userId = String(userId)
            requestParam.friendid = friendUser.userid
            
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                friendUser.isFollowed = "N"
                let cell = tableView.cellForRow(at: indexPath)
                cell?.accessoryView = UIImageView(image: self.imgCheckBoxBlank)
                
            }, failureBlock: {(error : Error?) in
                
            })
        }
    }    
}

extension FindFriendDetailsVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(FollowingCell.self)) as! FollowingCell
        
        let user = userList[indexPath.row]
        if !user.username.isEmpty {
            cell.lblUserName.text = user.username
        } else {
            cell.lblUserName.text = "Name not shared yet".localized()
        }
        
        if user.userimage != nil && URL(string: user.userimage) != nil {
            cell.ivUserImage.setImageWith(URL(string: user.userimage)!, placeholderImage: #imageLiteral(resourceName: "user_no_img"))           
        }
        cell.selectedBackgroundView = BlankView
        
        return cell
    }
}

class Debouncer: NSObject {
    
    var callback: (() -> ())
    var delay: Double
    weak var timer: Timer?
    
    init(delay: Double, callback: @escaping (() -> ())) {
        self.delay = delay
        self.callback = callback
    }
    
    func call() {
        timer?.invalidate()
        let nextTimer = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(Debouncer.fireNow), userInfo: nil, repeats: false)
        timer = nextTimer
    }
    
    func fireNow() {
        self.callback()
    }
}

