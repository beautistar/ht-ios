//
//  ViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit
import SlideMenuControllerSwift
import Localize_Swift
import AFNetworking
import FCUUID
import Toucan

class MainViewControllerCell : UICollectionViewCell {
    
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    
    //MARK: Memory Management Method
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Initialisation
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //imgItem.contentMode = .scaleAspectFit
        //lblItemName.numberOfLines = 0
        //lblItemName.sizeToFit()
    }
    
    //------------------------------------------------------
}

class MainViewController: BaseVC, PostVCDelegate, SearchDelegate {
    
    @IBOutlet var rightItemsView: UIView!
    @IBOutlet weak var collectionItemsView: UICollectionView!
    @IBOutlet weak var bottomIndicator: UIActivityIndicatorView!
    
    var productItems : [ProductListModal] = []
    var searchRequestParam : RequestParamModal?
    var isReachToEnd : Bool = false
    
    let pageSize = 9
    let preloadMargin = 1
    var lastLoadedPage = 0
    
    @IBOutlet weak var layoutBottomGuide: NSLayoutConstraint!
    
    lazy var viewFullScreen : UIView = {
        let viewFullScreen = UIView(frame: UIScreen.main.bounds)
        viewFullScreen.backgroundColor = kColorBackground
        return viewFullScreen
    }()
    
    lazy var refreshControl: UIRefreshControl = {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    //------------------------------------------------------
    
    //MARK: Memory Management Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        MyNotificationCenter.removeObserver(self)
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Method
    
    func configureUI() {
       
        self.title = "Home".localized()
        
        let rightItems = UIBarButtonItem(customView: rightItemsView)
        self.navigationItem.rightBarButtonItem = rightItems
        
        self.bottomIndicator.isHidden = true
    }
    
    func requestForProductList(page : NSInteger, showLoader : Bool)  {
        
        let request = "\(kAPIProductList)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIProductList
        requestParam.userId = String(userId)
        requestParam.page = String(page + 1)
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: false, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.layoutBottomGuide.constant = 0
            self.refreshControl.endRefreshing()
            if page == 0 {
                self.productItems.removeAll()
            }
            
            self.productItems.append(contentsOf: resposne.productList)
            self.bottomIndicator.isHidden = true
            self.collectionItemsView.reloadData()
            
        }, failureBlock: {(error : Error?) in
            
            self.layoutBottomGuide.constant = 0
            self.bottomIndicator.isHidden = true
        })
    }
    
    func requestForSignup() {
        
        let request = "\(kAPIRegister)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIRegister
        //requestParam.deviceId = UIDevice.current.identifierForVendor?.uuidString
        //requestParam.deviceId = UUID().uuidString
        requestParam.deviceId = FCUUID.uuidForDevice()
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            let currentUser = resposne.userInfo
            if currentUser != nil {
                
                if (currentUser!.userid != nil && currentUser!.userid != "0") {
                    MyUserDefaults.set(Int(currentUser!.userid), forKey: kCurrentUserId)
                }
                let userData = NSKeyedArchiver.archivedData(withRootObject: currentUser!.toDictionary())
                MyUserDefaults.setValue(userData, forKey: kCurrentUser)
                
                if currentUser?.username == "" {
                    
                    let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeEditProfile) as! EditProfileVC
                    self.present(controller, animated: false, completion: {
                        self.viewFullScreen.removeFromSuperview()
                    });
                }
                
            } else {
                
                let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeEditProfile) as! EditProfileVC
                self.present(controller, animated: false, completion: {
                    self.viewFullScreen.removeFromSuperview()
                });
            }
            self.requestForProductList(page: 0, showLoader: true)
            
        }, failureBlock: {(error : Error?) in
            
        })
    }
    
    func handleRefresh(refreshControl: UIRefreshControl) {
        
        isReachToEnd = false
        lastLoadedPage = 0
        requestForProductList(page: 0, showLoader : false)
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnNetworkTapped(_ sender: UIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeMyNetwords) as! MyNetworksVC
        controller.isMenuScreen = false
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnCameraTapped(_ sender: UIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypePost) as! PostVC
        controller.isMenuScreen = false
        controller.delegate = self
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: Notification
    
    func profileTapped(notification : Notification) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfile) as! ProfileVC
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: PostVCDelegate
    
    func refreshProductList() {
        
        self.requestForProductList(page: 0, showLoader: false)
    }
    
    //------------------------------------------------------
    
    //MARK: SearchDelegate
    
    func applyFilterOnProductList(productItems: [ProductListModal], requestParam: RequestParamModal) {

        self.searchRequestParam = requestParam
        self.productItems = productItems
        self.collectionItemsView.reloadData()
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if segue.identifier == "segueSearchVC" {
            
            let controller = segue.destination as! SearchVC        
            controller.requestParam = searchRequestParam
            controller.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionItemsView.isHidden = true
        
        MyNotificationCenter.addObserver(self, selector: #selector(profileTapped), name: NSNotification.Name(rawValue: kNotificationHomeVC), object: nil)
        
        MyNotificationCenter.addObserver(self, selector: #selector(configureUI), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        configureUI()
        
        self.collectionItemsView.addSubview(self.refreshControl)
        self.collectionItemsView.alwaysBounceVertical = true
        
        /*let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.itemSize = CGSize(width: width / 2, height: width / 2)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionItemsView!.collectionViewLayout = layout*/
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
            
            self.collectionItemsView.isHidden = false
            
            if userId == 0 {
                //self.requestForSignup()
            } else {
                self.requestForProductList(page: 0, showLoader: true)
            }
        })
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigationBarItem()
    }
}

extension MainViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (UIScreen.main.bounds.width - 30) / 2
        var height : CGFloat = width
        let product = productItems[indexPath.row]
        let myString: NSString = product.productName as NSString
        let size: CGSize = myString.size(attributes: [NSStrokeWidthAttributeName:width ,NSFontAttributeName: UIFont.systemFont(ofSize: 17.0)])
        height += size.height + 15
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfileDetail) as! ProfileDetailVC
        controller.product = productItems[indexPath.item]
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension MainViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.className(MainViewControllerCell.self), for: indexPath) as! MainViewControllerCell
        
        let product = productItems[indexPath.row]
        let imageURL = URL(string: product.productImage)
        if imageURL != nil {
            
            //cell.imgItem .setImageWith(imageURL!, placeholderImage: #imageLiteral(resourceName: "ic_menu_avatar"))
            let imgDownloadRequest = URLRequest(url: imageURL!)
            cell.imgItem .setImageWith(imgDownloadRequest, placeholderImage: #imageLiteral(resourceName: "ic_menu_avatar"), success: { (request : URLRequest, response : HTTPURLResponse?, image : UIImage) in
                
                //cell.imgItem.image = Toucan(image: image).resizeByCropping(cell.imgItem.bounds.size).image
                cell.imgItem.image = Toucan(image: image).resizeByCropping(cell.imgItem.bounds.size).image
                
            }, failure: { (request : URLRequest, response : HTTPURLResponse?, error : Error) in
                
            })
        }
        cell.lblItemName.text = product.productName
        
        return cell;
    }
}

extension MainViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if let indexPath = collectionItemsView.indexPathsForVisibleItems.last {
            
            let nextPage: Int = Int(indexPath.row / pageSize) + 1
            let preloadIndex = nextPage * (pageSize - preloadMargin)
            
            if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                
                layoutBottomGuide.constant = 50
                lastLoadedPage = nextPage
                bottomIndicator.isHidden = false
                requestForProductList(page: nextPage, showLoader: false)
            }
        }
    }
}

/*extension MainViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}*/
