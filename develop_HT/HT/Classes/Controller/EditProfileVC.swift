//
//  edit_profileVC.swift
//  HT
//
//  Created by Dharmesh on 06/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import Foundation
import UIKit
import Toucan
import MessageUI

class EditProfileVC : UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, GIDSignInUIDelegate, SAFBHelperDelegate, MFMessageComposeViewControllerDelegate {
    
    //------------------------------------------------------
    
    //MARK:- Outlets
    
    @IBOutlet weak var viewPrifileDetail: UIView!
    @IBOutlet weak var viewShare: UIView!
    @IBOutlet weak var btnChangePicture: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var lblFacebook: UILabel!
    @IBOutlet weak var lblGooglePlus: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtEnterName: UITextField!
    @IBOutlet weak var lblSomethingAbtYou: UILabel!
    @IBOutlet weak var txtSomethingAbtYou: UITextField!
    
    @IBOutlet weak var buildTrustAmongBuyers: UILabel!
    
    @IBOutlet weak var linkFacebook: UIButton!
    @IBOutlet weak var linkGoogle: UIButton!
    @IBOutlet weak var linkCall: UIButton!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgFacebook: UIImageView!
    @IBOutlet weak var imgGoogle: UIImageView!
    @IBOutlet weak var imgCall: UIImageView!
    
    let imagePicker = UIImagePickerController()
    var userProfile : UserProfile!
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        MyNotificationCenter.removeObserver(self)
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
        
        imagePicker.delegate = self
        
        LocalizedControl(view: btnChangePicture, key : "CHANGE PICTURE")
        LocalizedControl(view: btnUpdate, key : "UPDATE")
        LocalizedControl(view: lblFacebook, key : "Facebook")
        LocalizedControl(view: lblGooglePlus, key : "Google+")
        LocalizedControl(view: lblPhone, key : "Phone")
        LocalizedControl(view: lblName, key : "Name")
        LocalizedControl(view: lblSomethingAbtYou, key : "Something about you")
        
        LocalizedControl(view: txtEnterName, key : "Enter name")
        LocalizedControl(view: txtSomethingAbtYou, key : "i.e I love biking and hiking")
        LocalizedControl(view: buildTrustAmongBuyers, key : "BUILD TRUST AMONG BUYERS")
        
        LocalizedControl(view: linkFacebook, key : "LINK")
        LocalizedControl(view: linkGoogle, key : "LINK")
        LocalizedControl(view: linkCall, key : "LINK")
        
        if userData != nil {
            
            if userData!.userimage != nil && URL(string: userData!.userimage) != nil {
                //imgUser.setImageWith(URL(string: userData!.userimage)!, placeholderImage: #imageLiteral(resourceName: "user_no_img"))
                let imgDownloadRequest = URLRequest(url:URL(string: userData!.userimage)!)
                imgUser.setImageWith(imgDownloadRequest, placeholderImage: #imageLiteral(resourceName: "user_no_img"), success: { (request : URLRequest, response : HTTPURLResponse?, image : UIImage) in
                    
                    //self.imgUser.image = Toucan(image: image).maskWithRoundedRect(cornerRadius: 5).image
                    self.imgUser.image = Toucan(image: image).resizeByCropping(self.imgUser.bounds.size).image
                    
                }, failure: { (request : URLRequest, response : HTTPURLResponse?, error : Error) in
                    
                })
            } else if (userData!.imgProfile != nil) {
                imgUser.image = userData!.imgProfile
            } else {
                imgUser.image = #imageLiteral(resourceName: "user_no_img")
            }
        } else {
            imgUser.image = #imageLiteral(resourceName: "user_no_img")
        }
        
        updateVerifyLink()
    }
    
    //-------------------------------------------------------
    
    func setUp () {
        
        let currentUser = userData
        txtEnterName.text = currentUser?.username        
    }
    
    func updateVerifyLink() {
        
        if userProfile.verifyFacebook == "Y" {
            imgFacebook.image = #imageLiteral(resourceName: "ic_social_facebook_active")
        } else {
            imgFacebook.image = #imageLiteral(resourceName: "ic_social_facebook_negative")
        }
        
        if userProfile.verifyGoogle == "Y" {
            imgGoogle.image = #imageLiteral(resourceName: "ic_social_google_active")
        } else {
            imgGoogle.image = #imageLiteral(resourceName: "ic_social_google_negative")
        }
        
        if userProfile.isVerified == 1 {
            imgCall.image = #imageLiteral(resourceName: "ic_social_phone_active")
        } else {
            imgCall.image = #imageLiteral(resourceName: "ic_social_phone_negative")
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnChangePictureTaped(_ sender: UIButton) {
        
        let actionsheet = UIAlertController(title: kAppName, message: "Select Photo".localized(), preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let actionGallery = UIAlertAction(title: kGallery, style: UIAlertActionStyle.default, handler: {( action : UIAlertAction) in
            
            if (UIImagePickerController.isSourceTypeAvailable(.photoLibrary)) {
                
                self.imagePicker.sourceType = .photoLibrary
                self.present(self.imagePicker, animated: true, completion: {
                })
                
            } else {
                
                messageBox(message: "Photo gallery unsupported")
            }
        })
        actionsheet.addAction(actionGallery)
        
        let actionCamera = UIAlertAction(title: kCamera, style: UIAlertActionStyle.default, handler: {( action : UIAlertAction) in
            
            if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
                self.imagePicker.sourceType = .camera
                self.present(self.imagePicker, animated: true, completion: {
                })
            } else {
                messageBox(message: "Camera unsupported")
            }
        })
        actionsheet.addAction(actionCamera)
        
        let actionCanel = UIAlertAction(title: kCancel, style: UIAlertActionStyle.cancel, handler: {( action : UIAlertAction) in
        })
        actionsheet.addAction(actionCanel)
        
        present(actionsheet, animated: true, completion: nil)
    }
    
    @IBAction func btnFacebookTapped(_ sender: AnyObject) {
        
        AppDelegate.singleton.linkType = 1
        
        let fbHelper:SAFBHelper? = SAFBHelper()
        fbHelper?.delegate = self
        fbHelper?.loginToFacebook()
    }
    
    //------------------------------------------------------
    
    @IBAction func btnGPlusTapped(_ sender: AnyObject) {
        
        AppDelegate.singleton.linkType = 2
        
        GIDSignIn.sharedInstance().uiDelegate = self
        //GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func btnCallTapped(_ sender: UIButton) {
        
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Message Body"
            controller.recipients = [""]
            controller.messageComposeDelegate = self
            present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnUpdateTaped(_ sender: UIButton) {
        
        if txtEnterName.text?.isEmpty == true {
            messageBox(message: "Please enter name")
            return
        }

        if txtSomethingAbtYou.text?.isEmpty == true {
            messageBox(message: "Please enter something about you")
            return
        }        
        
        if imgUser.image == nil {
            messageBox(message: "Please select picture")
            return
        }
        
        let currentUser = UserModal(fromDictionary: [:])
        currentUser.username = txtEnterName.text
        currentUser.aboutYou = txtSomethingAbtYou.text
        currentUser.imgProfile = cropToBounds(image: imgUser.image!, width: 500, height: 500)
        
        let request = "\(kAPIUpdateUser)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIUpdateUser
        requestParam.username = currentUser.username
        requestParam.userId = String(userId)
        
        var showLoader = true
        
        if self.isModal() {
            showLoader = false
        }
        
        RequestManager.singleton.requestMultipartPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: showLoader, name: "user_photo", images: [currentUser.imgProfile], successBlock: { (resposne :MessageModal, message : String?) in
           
            if resposne.resultCode == 0 {
                
                let userData = NSKeyedArchiver.archivedData(withRootObject: currentUser.toDictionary())
                MyUserDefaults.setValue(userData, forKey: kCurrentUser)
                
                if self.isModal() {
                    self.dismiss(animated: true, completion: {})
                } else {
                    _ = self.navigationController?.popViewController(animated: true)
                }
                
            } else {
                
                if message != nil {
                    messageBox(message: message)
                } else {
                    messageBox(message: "Please try again")
                }
            }
            
        }, failureBlock: {(error : Error?) in
            
            messageBox(message: error?.localizedDescription)
        })
    }
    
    //------------------------------------------------------
    
    //MARK: UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let cropImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        imgUser.image = cropImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: GIDSignInUIDelegate
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        present(viewController, animated: true) {
        }
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        dismiss(animated: true) {
        }
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
        if error != nil {
            messageBox(message: error.localizedDescription)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: SAFBHelperDelegate
    
    func fbUserFailedToLogIn(error:NSError) {
        messageBox(message: error.localizedDescription)
    }
    
    func fbUserLoggedIn(dicParams: NSMutableDictionary) {
        
        MyNotificationCenter.post(name: NSNotification.Name(rawValue: kNotificationVerifyLink), object: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: MFMessageComposeViewControllerDelegate
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        dismiss(animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: Notification
    
    func notificationVerified(notification : Notification) {
        
        if AppDelegate.singleton.linkType == 1 {//facebook
            
            let request = "\(kAPIVerifyFacebook)"
            
            let requestParam = RequestParamModal()
            requestParam.action = kAPIVerifyFacebook
            requestParam.userId = String(userId)
            
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: false, successBlock: { (resposne :MessageModal, message : String?) in
                
                if resposne.resultCode == 0 {
                    self.userProfile.verifyFacebook = "Y"
                    self.updateVerifyLink()
                }
                
            }, failureBlock: {(error : Error?) in
                
            })
            
        } else if (AppDelegate.singleton.linkType == 2) { //google
            
            let request = "\(kAPIVerifyGoogle)"
            
            let requestParam = RequestParamModal()
            requestParam.action = kAPIVerifyGoogle
            requestParam.userId = String(userId)
            
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: false, successBlock: { (resposne :MessageModal, message : String?) in
                
                if resposne.resultCode == 0 {
                    self.userProfile.verifyGoogle = "Y"
                    self.updateVerifyLink()
                }

            }, failureBlock: {(error : Error?) in
                
            })
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MyNotificationCenter.addObserver(self, selector: #selector(notificationVerified), name: NSNotification.Name(rawValue: kNotificationVerifyLink), object: nil)
        configureUI()
        setUp()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
    
}
