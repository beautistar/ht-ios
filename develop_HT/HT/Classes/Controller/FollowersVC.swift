//
//  FollowersVC.swift
//  HT
//
//  Created by Dharmesh on 03/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class FollowersVC : BaseVC {
    
    @IBOutlet weak var viewNoFollowers: UIView!
    @IBOutlet weak var tblFollowers: UITableView!
    
    @IBOutlet weak var lblDontHaveFollowers: UILabel!
    @IBOutlet weak var lblChatPostFollow: UILabel!
    @IBOutlet weak var btnInviteFriends: UIButton!
    
    var userList : [UserList] = []
    
    let imgCheckBoxSelected = #imageLiteral(resourceName: "ic_following_list")
    let imgCheckBoxBlank = #imageLiteral(resourceName: "ic_add_following")
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {

        LocalizedControl(view: lblDontHaveFollowers, key : "You don't have any followers yet")
        LocalizedControl(view: lblChatPostFollow, key : "Chat, post or follow to let others find you.")
        LocalizedControl(view: btnInviteFriends, key : "Invite Friends")
        
        tblFollowers.isHidden = true
        self.tblFollowers.addSubview(self.refreshControl)
    }
    
    func handleRefresh(refreshControl: UIRefreshControl) {
        
        requestForFollowers(showLoader: false)
    }
    
    func requestForFollowers(showLoader: Bool) {
        
        let request = "\(kAPIFollowers)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIFollowers
        requestParam.userId = String(userId)
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: showLoader, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.tblFollowers.isHidden = false
            self.userList = resposne.userList
            self.tblFollowers.reloadData()
            self.refreshControl.endRefreshing()
            
        }, failureBlock: {(error : Error?) in
            
            self.tblFollowers.isHidden = false
            self.tblFollowers.reloadData()
            self.refreshControl.endRefreshing()
        })
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        requestForFollowers(showLoader: true)
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}

extension FollowersVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let user = userList[indexPath.row]
//        user.isFollowed = "N"
        if (user.isFollowed == "Y") {
            cell.accessoryView = UIImageView(image: imgCheckBoxSelected)
        } else {
            cell.accessoryView = UIImageView(image: imgCheckBoxBlank)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if userList.count == 0 {
            return self.view.bounds.height
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if userList.count == 0 {
            return viewNoFollowers
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let friendUser = userList[indexPath.row]
        
        if friendUser.isFollowed == "N" {
            
            let request = "\(kAPIFollowFriend)"
            
            let requestParam = RequestParamModal()
            requestParam.action = kAPIFollowFriend
            requestParam.userId = String(userId)
            requestParam.friendid = friendUser.userid
            
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                friendUser.isFollowed = "Y"
                let cell = tableView.cellForRow(at: indexPath) as! FollowingCell
                cell.accessoryView = UIImageView(image: self.imgCheckBoxSelected)
                
            }, failureBlock: {(error : Error?) in
                
            })
            
        } else {
            
            let request = "\(kAPIUnfollowFriend)"
            
            let requestParam = RequestParamModal()
            requestParam.action = kAPIUnfollowFriend
            requestParam.userId = String(userId)
            requestParam.friendid = friendUser.userid
            
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                friendUser.isFollowed = "N"
                let cell = tableView.cellForRow(at: indexPath)
                cell?.accessoryView = UIImageView(image: self.imgCheckBoxBlank)
                
            }, failureBlock: {(error : Error?) in
                
            })
        }
    }
}

extension FollowersVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(FollowingCell.self)) as! FollowingCell
        
        let user = userList[indexPath.row]
        if !user.username.isEmpty {
            cell.lblUserName.text = user.username
        } else {
            cell.lblUserName.text = "Name not shared yet".localized()
        }
        
        if user.userimage != nil && URL(string: user.userimage) != nil {
            cell.ivUserImage.setImageWith(URL(string: user.userimage)!, placeholderImage: #imageLiteral(resourceName: "user_no_img"))
        }
        cell.selectedBackgroundView = BlankView
        
        return cell
    }
}
