//
//  HelpVC.swift
//  HT
//
//  Created by Dharmesh on 02/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift

class HelpVC : BaseVC {
    
    @IBOutlet weak var lblHowCanWeHelp: UILabel!
    @IBOutlet weak var btnSendUsMail: UIButton!
    @IBOutlet weak var btnTermsAndCondition: UIButton!
    @IBOutlet weak var btnFAQs: UIButton!
    @IBOutlet weak var lblTermsAndConditions: UILabel!
    @IBOutlet weak var lblFAQ: UILabel!
    @IBOutlet weak var lblSafetyTips: UILabel!
    @IBOutlet weak var btnCopyRights: UIButton!
    @IBOutlet weak var lblSendUsMail: UILabel!
    @IBOutlet weak var lblTermsandConditions: UILabel!
    @IBOutlet weak var lblFAQs: UILabel!
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
        
        self.title = "Help".localized()
        
        LocalizedControl(view: lblHowCanWeHelp, key : "How can we help you?")
        //LocalizedControl(view: btnSendUsMail, key : "Send us an email")
        LocalizedControl(view: lblTermsandConditions, key : "Terms and Conditions")
        LocalizedControl(view: lblFAQ, key : "FAQs")
        
        LocalizedControl(view: lblSafetyTips, key : "Safety tips")
        LocalizedControl(view: btnCopyRights, key : "Ht - Copyright @ 2016")
        LocalizedControl(view: lblSendUsMail, key : "Send us an email")
        LocalizedControl(view: lblFAQs, key : "FAQs")
        LocalizedControl(view: lblTermsAndConditions, key : "Terms and Conditions")
        
    }
    
    func calcelDidTap(sender : UIBarButtonItem) {
        
        if (self.isModal()) {
            dismiss(animated: true, completion: nil)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        MyNotificationCenter.removeObserver(self)        
    }
    
    //------------------------------------------------------
    
    //MARK: Notification
    
    func profileTapped(notification : Notification) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfile) as! ProfileVC
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        
         MyNotificationCenter.addObserver(self, selector: #selector(profileTapped), name: NSNotification.Name(rawValue: kNotificationHelpVC), object: nil)
         MyNotificationCenter.addObserver(self, selector: #selector(configureUI), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (self.isModal()) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(calcelDidTap))
        } else {
            self.setNavigationBarItem()
        }
    }
    
    //------------------------------------------------------
}

