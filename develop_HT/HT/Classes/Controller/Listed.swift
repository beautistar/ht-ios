//
//  Listed.swift
//  HT
//
//  Created by Dharmesh on 04/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Toucan

class ListedVC : BaseVC, PostVCDelegate {
    
    @IBOutlet weak var viewNoRecords: UIView!
    @IBOutlet weak var collectionProduct: UICollectionView!
    
    @IBOutlet weak var lblHaventListed: UILabel!
    @IBOutlet weak var lblSellSomething: UILabel!
    @IBOutlet weak var btnPost: UIButton!
    
    var productItems : [ProductListModal] = []
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
                       
        LocalizedControl(view: lblHaventListed, key : "You haven't listed anything yet")
        LocalizedControl(view: lblSellSomething, key : "Sell anything you dont use anymore")
        LocalizedControl(view: btnPost, key : "Post")
    }
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnPostTapped(_ sender: UIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypePost) as! PostVC
        controller.isMenuScreen = false
        controller.delegate = self
        navigationController?.pushViewController(controller, animated: true)

    }
    
    //------------------------------------------------------
    
    //MARK: PostVCDelegate
    
    func refreshProductList() {
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}

extension ListedVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (UIScreen.main.bounds.width - 30) / 2
        var height : CGFloat = width
        let product = productItems[indexPath.row]
        let myString: NSString = product.productName as NSString
        let size: CGSize = myString.size(attributes: [NSStrokeWidthAttributeName:width ,NSFontAttributeName: UIFont.systemFont(ofSize: 17.0)])
        height += size.height + 15
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfileDetail) as! ProfileDetailVC
        controller.product = productItems[indexPath.item]
        controller.isMyProduct = true
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension ListedVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if productItems.count == 0 {
            collectionProduct.isHidden = true
        } else {
            collectionProduct.isHidden = false
        }
        return productItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.className(MainViewControllerCell.self), for: indexPath) as! MainViewControllerCell
        
        let product = productItems[indexPath.row]
        let imageURL = URL(string: product.productImage)
        if imageURL != nil {
            
            //cell.imgItem .setImageWith(imageURL!, placeholderImage: #imageLiteral(resourceName: "ic_menu_avatar"))
            let imgDownloadRequest = URLRequest(url: imageURL!)
            cell.imgItem .setImageWith(imgDownloadRequest, placeholderImage: #imageLiteral(resourceName: "ic_menu_avatar"), success: { (request : URLRequest, response : HTTPURLResponse?, image : UIImage) in
                
                //cell.imgItem.image = Toucan(image: image).maskWithRoundedRect(cornerRadius: 5).image
                cell.imgItem.image = Toucan(image: image).resizeByCropping(cell.imgItem.bounds.size).image
                
            }, failure: { (request : URLRequest, response : HTTPURLResponse?, error : Error) in
                
            })
        }
        cell.lblItemName.text = product.productName
        return cell;
    }
}

extension ListedVC : UIScrollViewDelegate {
    
    /*func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if let indexPath = collectionItemsView.indexPathsForVisibleItems.last {
            
            let nextPage: Int = Int(indexPath.row / pageSize) + 1
            let preloadIndex = nextPage * (pageSize - preloadMargin)
            
            if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
                
                layoutBottomGuide.constant = 50
                lastLoadedPage = nextPage
                bottomIndicator.isHidden = false
                requestForProductList(page: nextPage, showLoader: false)
            }
        }
    }*/
}

