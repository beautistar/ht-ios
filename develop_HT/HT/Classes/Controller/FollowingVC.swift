//
//  FollowingVC.swift
//  HT
//
//  Created by Dharmesh on 03/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation

protocol FollowingCellDelegate {
    
    func btnFriendActionTapped(sender: UIButton, cell : FollowingCell)
}

class FollowingCell : UITableViewCell {
    
    var delegate : FollowingCellDelegate?
    
    @IBOutlet weak var ivUserImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    override func awakeFromNib() {
        
        ivUserImage.layer.cornerRadius = 25
        ivUserImage.clipsToBounds = true
        
        let margin : CGFloat = 15
        let lineSeperator = UIView(frame: CGRect(x: margin, y: bounds.size.height - 1, width: ScreenRect.size.width - (margin * 2), height: 1))
        lineSeperator.backgroundColor = UIColor.lightGray
        addSubview(lineSeperator)
    }
}

class FollowingVC : BaseVC {

    @IBOutlet weak var viewNoFollowing: UIView!
    @IBOutlet weak var tblFollowing: UITableView!
    
    @IBOutlet weak var lblGetSomeFollowers: UILabel!
    @IBOutlet weak var btnInviteFriends: UIButton!
    
    var userList : [UserList] = []
    
    let imgCheckBoxSelected = #imageLiteral(resourceName: "ic_following_list")
    let imgCheckBoxBlank = #imageLiteral(resourceName: "ic_add_following")
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
        
        LocalizedControl(view: lblGetSomeFollowers, key : "Get some followers to let people know when you post!")
        LocalizedControl(view: btnInviteFriends, key : "Invite Friends")
    
        self.tblFollowing.isHidden = true
        self.tblFollowing.addSubview(self.refreshControl)
    }
    
    func handleRefresh(refreshControl: UIRefreshControl) {
        
        requestForFollowings(showLoader: false)
    }
    
    func requestForFollowings(showLoader : Bool) {
        
        let request = "\(kAPIFollowing)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIFollowing
        requestParam.userId = String(userId)
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: showLoader, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.userList = resposne.userList
            self.tblFollowing.isHidden = false
            
            self.tblFollowing.reloadData()
            self.refreshControl.endRefreshing()
            
        }, failureBlock: {(error : Error?) in
            
            self.tblFollowing.isHidden = false
            self.tblFollowing.reloadData()
            self.refreshControl.endRefreshing()
        })
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        requestForFollowings(showLoader: true)
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}

extension FollowingVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let user = userList[indexPath.row]
        user.isFollowed = "Y"
        if (user.isFollowed == "Y") {
            cell.accessoryView = UIImageView(image: imgCheckBoxSelected)
        } else {
            cell.accessoryView = UIImageView(image: imgCheckBoxBlank)
        }
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let friendUser = userList[indexPath.row]
        
        if friendUser.isFollowed == "N" {
            
            let request = "\(kAPIFollowFriend)"
            
            let requestParam = RequestParamModal()
            requestParam.action = kAPIFollowFriend
            requestParam.userId = String(userId)
            requestParam.friendid = friendUser.userid
            
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                friendUser.isFollowed = "Y"
                let cell = tableView.cellForRow(at: indexPath) as! FollowingCell
                cell.accessoryView = UIImageView(image: self.imgCheckBoxSelected)
                
            }, failureBlock: {(error : Error?) in
                
            })
            
        } else {
            
            let request = "\(kAPIUnfollowFriend)"
            
            let requestParam = RequestParamModal()
            requestParam.action = kAPIUnfollowFriend
            requestParam.userId = String(userId)
            requestParam.friendid = friendUser.userid
            
            RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
                
                friendUser.isFollowed = "N"
                let cell = tableView.cellForRow(at: indexPath)
                cell?.accessoryView = UIImageView(image: self.imgCheckBoxBlank)
                
            }, failureBlock: {(error : Error?) in
                
            })
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
        if userList.count == 0 {
            return self.view.bounds.height
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if userList.count == 0 {
            return viewNoFollowing
        } else {
            return nil
        }
    }
}

extension FollowingVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(FollowingCell.self)) as! FollowingCell
        
        let user = userList[indexPath.row]
        if !user.username.isEmpty {
            cell.lblUserName.text = user.username
        } else {
            cell.lblUserName.text = "Name not shared yet".localized()
        }
        
        if user.userimage != nil && URL(string: user.userimage) != nil {
            cell.ivUserImage.setImageWith(URL(string: user.userimage)!, placeholderImage: #imageLiteral(resourceName: "user_no_img"))
        }
        cell.selectedBackgroundView = BlankView
        
        return cell
    }
}

