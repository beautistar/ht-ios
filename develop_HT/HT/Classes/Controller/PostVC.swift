//
//  PostVC.swift
//  HT
//
//  Created by Dharmesh on 02/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import GrowingTextView
import DropDown
import MBProgressHUD
import Localize_Swift
import ImagePicker
import Photos

protocol PostCellDelegate {
    
    func btnRemoveTaped(cell : PostCell)
}

class PostCell : UICollectionViewCell {
    
    @IBOutlet weak var imgItem: UIImageView!
    
    var delegate : PostCellDelegate?
    
    //MARK: Memory Management Method
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Action Method
    
    @IBAction func btnRemoveTaped(_ sender: UIButton) {
        
        delegate?.btnRemoveTaped(cell: self)
    }
    
    //------------------------------------------------------
    
    //MARK: Initialisation
    
    override func prepareForReuse() {
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgItem.contentMode = .scaleAspectFill
        imgItem.clipsToBounds = true
    }
    
    //------------------------------------------------------
}

protocol PostReusableViewDelegate {
    
    func btnCameraTaped(sender: UIButton)
}

class PostReusableView : UICollectionReusableView {
    
    var delegate : PostReusableViewDelegate?
    
    @IBOutlet weak var btnCamera: UIButton!
    
    //MARK: Memory Management Method
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnCameraTaped(_ sender: UIButton) {
        delegate?.btnCameraTaped(sender: sender)
    }
    //------------------------------------------------------
    
    //MARK: Initialisation
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //------------------------------------------------------
}

protocol PostVCDelegate {

    func refreshProductList()
}

class PostVC : BaseVC, UINavigationControllerDelegate, GrowingTextViewDelegate, PostReusableViewDelegate, PostCellDelegate, ImagePickerDelegate, CLLocationManagerDelegate, UITextFieldDelegate, MapVCDelegate {
    
    var delegate : PostVCDelegate?
    
    @IBOutlet weak var txtViewDescription: GrowingTextView!
   
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var imgDownArrow: UIImageView!
    @IBOutlet weak var lblWhatDoYouWantToSell: UILabel!
    @IBOutlet weak var lblSelling: UILabel!
    @IBOutlet weak var txtSellingItem: UITextField!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCannotBeShorter: UILabel!
    @IBOutlet weak var lblChooseCategory: UILabel!
    @IBOutlet weak var txtHowMuch: UITextField!
    @IBOutlet weak var lblShownLocation: UILabel!
    @IBOutlet weak var btnPost: UIButton!
    
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var mapProduct: MKMapView!
    
    let imagePicker = UIImagePickerController()
    //let imagePickerController = BSImagePickerViewController()
    
    var isMenuScreen : Bool = true
    
    let dropDownClass = DropDown()
    let requestParam = RequestParamModal()
    
    var productImages : [UIImage] = []
    var hud : MBProgressHUD!
    let manager = PHImageManager.default()
    
    var locationManager: CLLocationManager! = CLLocationManager()
    var locationCoordinate2D : CLLocationCoordinate2D? = nil
    var isLocationDidChange : Bool = false
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
        locationManager.stopUpdatingLocation()
        MyNotificationCenter.removeObserver(self)
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
        
        //imagePicker.delegate = self
        self.title = "Post".localized()
        txtHowMuch.delegate = self
        
        MyNotificationCenter.addObserver(self, selector: #selector(profileTapped), name: NSNotification.Name(rawValue: kNotificationPostVC), object: nil)
        
        txtViewDescription.maxLength = 150
        txtViewDescription.trimWhiteSpaceWhenEndEditing = false
        txtViewDescription.placeHolder = "i.e My first bicycle, year 2015, in perfect condition".localized() as NSString
        txtViewDescription.placeHolderColor = UIColor(white: 0.8, alpha: 1.0)
        txtViewDescription.maxHeight = 200.0
        txtViewDescription.backgroundColor = UIColor.clear
        txtViewDescription.layer.cornerRadius = 4.0
        txtViewDescription.delegate = self
        
        dropDownClass.dataSource = ["Electronics".localized(), "Fashion & Accessories".localized(), "Sports & Hobbies".localized(), "Home & Garden".localized(), "File, media and books".localized(), "Babies & Kids".localized(), "Gaming".localized(), "Vehicles".localized(), "Others".localized()]
        
        btnCategory .setTitle(dropDownClass.dataSource.first, for: .normal)
        self.requestParam.category = dropDownClass.dataSource.first
        
        dropDownClass.anchorView = btnCategory
        dropDownClass.selectionAction = { [unowned self] (index, item) in
            
            self.requestParam.category = item
            self.btnCategory .setTitle(item, for: .normal)
        }
        dropDownClass.backgroundColor = UIColor.white
        dropDownClass.textColor = UIColor.black
        dropDownClass.separatorColor = kColorGray
        
        LocalizedControl(view: lblPrice, key : "Price")
        LocalizedControl(view: lblSelling, key : "Selling")
        LocalizedControl(view: lblShownLocation, key : "Add location shown")
        LocalizedControl(view: lblChooseCategory, key : "Choose a category")
        LocalizedControl(view: lblCannotBeShorter, key : "Cannot be shorter than 15 Characters")
        LocalizedControl(view: lblDescription, key: "Description")
        LocalizedControl(view: lblWhatDoYouWantToSell, key : "WHAT DO YOU WANT TO SELL TODAY ?")
        LocalizedControl(view: txtHowMuch, key : "How much is it?")
        LocalizedControl(view: btnPost, key : "POST")
        LocalizedControl(view: txtSellingItem, key : "i.e 21 speed 26' Bicycle")
        
        //location
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch(CLLocationManager.authorizationStatus()) {
            
            case .notDetermined, .restricted, .denied: break
        
            case .authorizedAlways, .authorizedWhenInUse:
                self.locationManager.startUpdatingLocation()
                break
            }
        } else {
            locationManagerPopup()
        }
    }
    
    func addAnnotation(title : String, coordinate : CLLocationCoordinate2D) {
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = title
        self.mapProduct.addAnnotation(annotation)
        
        let miles : Double = 5.0;
        let scalingFactor : Double = abs(cos(2 * M_PI * coordinate.latitude / 360.0))
        
        var span : MKCoordinateSpan = MKCoordinateSpan()
        
        span.latitudeDelta = miles/69.0;
        span.longitudeDelta = miles/(scalingFactor * 69.0);
        
        var region : MKCoordinateRegion = MKCoordinateRegion()
        region.span = span;
        region.center = coordinate;
        
        self.mapProduct?.setRegion(region, animated: false)
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 800, height: 800), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    func locationManagerPopup() {
        
        let alertController = UIAlertController (title: kAppName, message: "Your location service currently disable, Do you wants to enable location service?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: kSettings, style: .default) { (_) -> Void in
            
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: kCancel, style: .default, handler: { (action :UIAlertAction) in
            
            if self.isMenuScreen {
                
            } else {
                //_ = self.navigationController?.popViewController(animated: true)
            }
        })
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnCategoryTapped(_ sender: UIButton) {
        //var image = UIImage(CGImage: img.CGImage, scale: 1.0, orientation: .DownMirrored)
        dropDownClass.show()
    }
    
    @IBAction func btnPostTapped(_ sender: UIButton) {
        
        if txtSellingItem.text?.isEmpty == true {
            messageBox(message: "Please enter selling name")
            return
        }
        
        if txtViewDescription.text.characters.count <= 15 {
            messageBox(message: "Product description cannot be shorter than 15 characters")
            return
        }
        
        if txtHowMuch.text?.isEmpty == true {
            messageBox(message: "Please enter price")
            return
        }
        
        if productImages.count == 0 {
            messageBox(message: "Please select image")
            return
        }
        
        if locationCoordinate2D == nil {
            locationManagerPopup()
            return
        }
        
        let request = "\(kAPIPostAdd)"
        
        requestParam.action = kAPIPostAdd
        requestParam.userId = String(userId)
        requestParam.productName = txtSellingItem.text
        requestParam.productDescription = txtViewDescription.text
        requestParam.productPrice = txtHowMuch.text
        requestParam.latitude = String(locationCoordinate2D!.latitude)
        requestParam.longitude = String(locationCoordinate2D!.longitude)
        
        RequestManager.singleton.requestMultipartPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, name: "product_image[]", images: productImages, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.delegate?.refreshProductList()
            
            if message != nil {
                messageBox(message: message)
            }
            
            
            let leftViewController = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeMenu)
            let navigationController = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeHome)
            let slideMenuController = ExSlideMenuController(mainViewController:navigationController, leftMenuViewController: leftViewController)
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            AppDelegate.singleton.window?.backgroundColor = kColorBackground
            AppDelegate.singleton.window?.tintColor = UIColor.yellow
            AppDelegate.singleton.window?.rootViewController = slideMenuController
            AppDelegate.singleton.window?.makeKeyAndVisible()
            
            /*if self.isMenuScreen {
                
                self.txtSellingItem.text = ""
                self.txtViewDescription.text = ""
                self.txtHowMuch.text = ""
                self.productImages = []
                self.collectionImages.reloadData()
                
            } else {
                _ = self.navigationController?.popViewController(animated: true)
            }*/
            
        }, failureBlock: {(error : Error?) in
            
        })
    }

    //------------------------------------------------------
    
    //MARK: PostCellDelegate
    
    func btnRemoveTaped(cell: PostCell) {
        
        let indexPath = collectionImages.indexPath(for: cell)
        productImages.remove(at: (indexPath?.row)!)
        collectionImages.reloadData()
    }
    
    //------------------------------------------------------
    
    //MARK: PostReusableViewDelegate
    
    func btnCameraTaped(sender: UIButton) {
        
        /*let actionsheet = UIAlertController(title: kAppName, message: "Select Photo".localized(), preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let actionGallery = UIAlertAction(title: kGallery, style: UIAlertActionStyle.default, handler: {( action : UIAlertAction) in
            
            if (UIImagePickerController.isSourceTypeAvailable(.photoLibrary)) {
                
                /*self.imagePicker.sourceType = .photoLibrary
                 self.present(self.imagePicker, animated: true, completion: {
                 })*/
                
                self.hud = progressHUD(view: self.view)
                self.hud.show(animated: true)
                self.imagePickerController.maxNumberOfSelections = 5
                self.imagePickerController.takePhotos = true
                
                self.productImages.removeAll()
                self.bs_presentImagePickerController(self.imagePickerController, animated: true,
                                                     select: { (asset: PHAsset) -> Void in
                                                        
                                                        //self.productImages.append(asset)
                                                        let selectedIamge = self.getAssetThumbnail(asset: asset)
                                                        self.productImages.append(selectedIamge)
                                                        self.collectionImages.reloadData()
                                                        
                }, deselect: { (asset: PHAsset) -> Void in

                    
                }, cancel: { (assets: [PHAsset]) -> Void in
                    
                    self.collectionImages.reloadData()
                    
                }, finish: { (assets: [PHAsset]) -> Void in
                    
                    self.collectionImages.reloadData()
                    
                }, completion: {
                })
                
            } else {
                
                messageBox(message: "Photo gallery unsupported")
            }
        })
        actionsheet.addAction(actionGallery)
        
        let actionCamera = UIAlertAction(title: kCamera, style: UIAlertActionStyle.default, handler: {( action : UIAlertAction) in
            
            if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
                self.imagePicker.sourceType = .camera
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: {
                })
            } else {
                messageBox(message: "Camera unsupported")
            }
        })
        actionsheet.addAction(actionCamera)
        
        let actionCanel = UIAlertAction(title: kCancel, style: UIAlertActionStyle.cancel, handler: {( action : UIAlertAction) in
        })
        actionsheet.addAction(actionCanel)
        
        present(actionsheet, animated: true, completion: nil)*/
        
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 6
        present(imagePickerController, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: Notification
    
    func profileTapped(notification : Notification) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfile) as! ProfileVC
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //let cropImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        //let cropImage = cropToBounds(image: image!, width: 400, height: 400)
        //requestParam.productImage = cropImage
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
        productImages = images
        collectionImages.reloadData()
        imagePicker.dismiss(animated: true, completion: {})
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: {})
    }
    
    //------------------------------------------------------
    
    //MARK: CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if (CLLocationCoordinate2DIsValid(locations[0].coordinate) && isLocationDidChange == false) {
           
            if locationCoordinate2D?.latitude != locations[0].coordinate.latitude && locationCoordinate2D?.longitude != locations[0].coordinate.longitude {
                
                mapProduct.removeAnnotations(mapProduct.annotations)
                locationCoordinate2D = locations[0].coordinate
                addAnnotation(title: "Current location".localized(), coordinate: locationCoordinate2D!)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if text == "" {
            return true
        }
        
        if text.characters.count > 6 {
            return false
        }
        
        if Int(text) != nil {
            return true
        } else {
            return false
        }
    }
    
    //------------------------------------------------------
    
    //MARK: MapVCDelegate
    
    func annotationDidChange(locationCoordinate2D: CLLocationCoordinate2D) {
        
        isLocationDidChange = true
        self.locationCoordinate2D = locationCoordinate2D
        mapProduct.removeAnnotations(self.mapProduct.annotations)
        addAnnotation(title: "Current location".localized(), coordinate: locationCoordinate2D)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueMapVC" {
            
            let controller = segue.destination as! MapVC
            controller.productName = txtSellingItem.text
            controller.locationCoordinate2D = locationCoordinate2D
            controller.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        
        MyNotificationCenter.addObserver(self, selector: #selector(configureUI), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isMenuScreen {
            self.setNavigationBarItem()
        }
    }
    
    //------------------------------------------------------
}

extension PostVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width : CGFloat = 72
        let height : CGFloat = 72
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension PostVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return productImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            
            let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: String.className(PostReusableView.self), for: indexPath) as! PostReusableView
            reusableView.delegate = self
            return reusableView
            
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.className(PostCell.self), for: indexPath) as! PostCell
        cell.delegate = self
        
        /*let option = PHImageRequestOptions()
        
        option.isSynchronous = true
        let asset : PHAsset = productImages[indexPath.row]
        
        let finalRequestOptions = PHImageRequestOptions()
        
        manager.requestImage(for: asset,
                                     targetSize: CGSize(width: 500, height: 500)/*PHImageManagerMaximumSize*/,
                                     contentMode: .aspectFit,
                                     options: finalRequestOptions) { (finalResult, _) in
                                        
                                        cell.imgItem.image = finalResult
                                        self.hud.hide(animated: true)
        }*/
        
        cell.imgItem.image = productImages[indexPath.row]
        //self.hud.hide(animated: true)
        return cell;
    }
}
