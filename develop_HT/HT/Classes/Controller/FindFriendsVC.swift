//
//  FindFriendsVC.swift
//  HT
//
//  Created by Dharmesh on 03/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift

class FindFriendsVC : BaseVC {
    
    @IBOutlet weak var btnFindFriendsinHT: UIButton!
    
    @IBOutlet weak var lblFindFriends: UILabel!
    @IBOutlet weak var lblFindFriendFB: UILabel!
    @IBOutlet weak var lblThereNoFriends: UILabel!
    @IBOutlet weak var lblTryAddingFriend: UILabel!
    @IBOutlet weak var btnInviteFriends: UIButton!    
    @IBOutlet weak var lblInviteFriends: UILabel!        
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
        
        //LocalizedControl(view: btnFindFriendsinHT, key : "Find friends in ht..")
        LocalizedControl(view: lblFindFriends, key : "Find friends in ht..")
        LocalizedControl(view: lblFindFriendFB, key : "Find friends from Facebook...")
        LocalizedControl(view: lblThereNoFriends, key : "there are no new friends on ht...")
        LocalizedControl(view: lblTryAddingFriend, key : "Try adding friends from a different source or sharing ht.")
        LocalizedControl(view: btnInviteFriends, key : "INVITE FRIENDS")
        LocalizedControl(view: lblInviteFriends, key : "Invite friends to ht")
    }       
    
    //------------------------------------------------------
    
    //MARK:- Action Methods
    
    @IBAction func btnFindFriendsInHTTapped(_ sender: AnyObject) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeFindFriendsDetail) as! FindFriendDetailsVC
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //------------------------------------------------------
    
    @IBAction func btnFacebookTapped(_ sender: AnyObject) {
    }
    
    //------------------------------------------------------
    
    @IBAction func btnInviteFriendsTapped(_ sender: AnyObject) {
    }
    
    //------------------------------------------------------
    
    @IBAction func btnShareAppTapped(_ sender: AnyObject) {
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MyNotificationCenter.addObserver(self, selector: #selector(configureUI), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        configureUI()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
