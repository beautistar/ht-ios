//
//  ProductListLayout.swift
//  HT
//
//  Created by Dharmesh on 16/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import Foundation

import UIKit
import Foundation

class  ProductListLayout : UICollectionViewLayout {
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    
        let attribute = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        
        attribute.frame = CGRect(x: 0, y: 0, width: 320, height: 320)
        return attribute
    }
    
    //------------------------------------------------------
    
    //MARK: Initialisation
    
    override func prepare() {
        
        
    }
    
    //------------------------------------------------------
}
