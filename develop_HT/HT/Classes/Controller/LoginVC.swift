//
//  LoginVC.swift
//  HT
//
//  Created by Dharmesh on 04/02/17.
//  Copyright © 2017 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class LoginVC : BaseVC {
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var viewFullName: UIView!
    @IBOutlet weak var txtFullname: UITextField!
    @IBOutlet weak var btnSignup: UIButton!
    
    @IBOutlet weak var btnNewUser: UIButton!
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
        
        viewFullName.isHidden = true
        
        LocalizedControl(view: txtEmail, key: "Email")
        LocalizedControl(view: txtPassword, key: "Password")
        LocalizedControl(view: txtFullname, key: "Full name")
        LocalizedControl(view: btnSignup, key: "Sign In")
        
        let attrs = [
            NSFontAttributeName : UIFont.systemFont(ofSize: 19.0),
            NSForegroundColorAttributeName : UIColor.darkGray,
            NSUnderlineStyleAttributeName : 1] as [String : Any]
        
        let attributedString = NSMutableAttributedString(string:"")
        
        let buttonTitleStr = NSMutableAttributedString(string:"New user? Sign up", attributes:attrs)
        attributedString.append(buttonTitleStr)
        btnNewUser.setAttributedTitle(attributedString, for: .normal)
        
    }
    
    func IsValidInputs() -> Bool {
        
        if txtEmail.text?.isEmpty == true {
            messageBox(message: "Please enter an email")
            return false
        }
        
        if ValidationManager.singleton.isValidEmail(email: txtEmail.text!) == false {
            messageBox(message: "Please enter valid email")
            return false
        }
        
        if txtPassword.text?.isEmpty == true {
            messageBox(message: "Please enter password")
            return false
        }
        
        if ValidationManager.singleton.isValidPassword(password: txtPassword.text!) == false {
            messageBox(message: "Please enter valid password")
            return false
        }
        
        /*if segment.selectedSegmentIndex == 0 {//extra check validation for signup
            
            if txtFullname.text?.isEmpty == true {
                messageBox(message: "Please enter fullname")
                return false
            }
            
            if ValidationManager.singleton.isValidFullName(string: txtFullname.text!) == false {
                messageBox(message: "Please enter valid fullname")
                return false
            }
        }*/
        
        return true
    }
    
    func requestForSignup() {
        
        let request = "\(kAPIRegister)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPIRegister
        requestParam.email = txtEmail.text
        requestParam.password = txtPassword.text
        requestParam.username = txtFullname.text
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            if resposne.resultCode == 0 {
                
                MyUserDefaults.removeObject(forKey: kCurrentUser);
                MyUserDefaults.set(Int(resposne.id), forKey: kCurrentUserId)
                MyUserDefaults.synchronize()
                NavigationManager.singleton.setupHome()
                
            } else {
                
                messageBox(message: resposne.error)
            }
            
        }, failureBlock: {(error : Error?) in
            
        })
    }
    
    func requestForLogin() {
        
        let request = "\(kAPILogin)"
        
        let requestParam = RequestParamModal()
        requestParam.action = kAPILogin
        requestParam.email = txtEmail.text
        requestParam.password = txtPassword.text
        
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            if resposne.resultCode == 0 {
                
                MyUserDefaults.removeObject(forKey: kCurrentUser);
                let rUserId = resposne.userProfile.userid
                
                let userModal = UserModal(fromDictionary: [:])
                userModal.userid =  resposne.userProfile.userid
                userModal.userimage =  resposne.userProfile.userimage
                userModal.username = resposne.userProfile.username
                
                let userData = NSKeyedArchiver.archivedData(withRootObject: userModal.toDictionary())
                MyUserDefaults.setValue(userData, forKey: kCurrentUser)
                
                MyUserDefaults.set(Int(rUserId!), forKey: kCurrentUserId)
                MyUserDefaults.synchronize()
                NavigationManager.singleton.setupHome()
                
            } else {
                
                messageBox(message: resposne.error)
            }
            
        }, failureBlock: {(error : Error?) in
            
        })
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnHelpTapped(_ sender: UIButton) {
        
        let navigationHelpVC =  NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeHelp)
        present(navigationHelpVC, animated: true, completion: nil)
    }
    
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        
        let index = sender.selectedSegmentIndex
        
        if index == 0 {
            viewFullName.isHidden = false
            LocalizedControl(view: btnSignup, key: "Sign Up")
        } else {
            viewFullName.isHidden = true
            LocalizedControl(view: btnSignup, key: "Login")
        }
    }
    
    @IBAction func btnSignupTapped(_ sender: UIButton) {
        
        if IsValidInputs() == false {
            return
        }
        requestForLogin()
    }
    
    @IBAction func btnNewUserTapped(_ sender: UIButton) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeSignup) as! SignupVC
        _ = navigationController?.pushViewController(controller, animated: true)
        
    }
  
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
