//
//  SearchVC.swift
//  HT
//
//  Created by Dharmesh on 02/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation

protocol SearchDelegate {
    
    func applyFilterOnProductList(productItems : [ProductListModal], requestParam : RequestParamModal)
}

class SearchVC : BaseVC, UITextFieldDelegate {
    
    @IBOutlet weak var txtFromPrice: UITextField!
    @IBOutlet weak var txtToPrice: UITextField!
    
    @IBOutlet weak var tblSearch: UITableView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    
    var delegate : SearchDelegate?
    var requestParam : RequestParamModal?
    
    var selectedIndexes : [String] = []
    
    let imgCheckBoxSelected = #imageLiteral(resourceName: "checkbox_selected")
    let imgCheckBoxBlank = #imageLiteral(resourceName: "checkbox_blank")
    
    var items = ["Electronics", "Car and Motors", "Sports, Leisure and Games", "Home and Garden", "Movie, Books and Music", "Fashion and Accessories"]
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
        
        LocalizedControl(view: btnSearch, key : "SEARCH")
        LocalizedControl(view: lblPrice, key : "Price")
        LocalizedControl(view: lblFrom, key : "From")
        LocalizedControl(view: lblTo, key : "To")
        LocalizedControl(view: lblCategory, key : "Category")
        
        requestParam = requestParam ?? RequestParamModal()
        if requestParam?.category != nil {
            selectedIndexes = (requestParam?.category.components(separatedBy: ",")) ?? []
        } else {
            selectedIndexes = []
        }
        
        txtToPrice.text = requestParam?.priceTo
        txtFromPrice.text = requestParam?.priceFrom
    }
    
    func requestForProductList(showLoader : Bool)  {
        
        let request = "\(kAPISearch)"
        
        requestParam!.action = kAPISearch
        requestParam!.priceFrom = txtFromPrice.text
        requestParam!.priceTo = txtToPrice.text
        
        if selectedIndexes.count == 0 {
            messageBox(message: "Please select category(ies)")
            return
        }
        
        requestParam!.category = selectedIndexes.joined(separator: ",")
        RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam!.toDictionary(), showLoader: true, successBlock: { (resposne :MessageModal, message : String?) in
            
            self.delegate?.applyFilterOnProductList(productItems: resposne.productList, requestParam: self.requestParam!)
            _ = self.navigationController?.popViewController(animated: true)
            
        }, failureBlock: {(error : Error?) in
            
             self.delegate?.applyFilterOnProductList(productItems: [], requestParam: self.requestParam!)
            _ = self.navigationController?.popViewController(animated: true)
        })
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnSearchTap(_ sender: UIButton) {
        
        if txtFromPrice.text?.isEmpty == true {
            messageBox(message: "Please enter price from")
            return
        }
        
        if txtToPrice.text?.isEmpty == true {
            messageBox(message: "Please enter price to")
            return
        }
        
        requestForProductList(showLoader: true)
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if text == "" {
            return true
        }
        
        if text.characters.count > 6 {
            return false
        }
        
        if Int(text) != nil {
            return true
        } else {
            return false
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        configureUI()        
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}

extension SearchVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (selectedIndexes.contains(String(indexPath.row+1))) {
            cell.textLabel?.textColor = kColorRed
            cell.accessoryView = UIImageView(image: imgCheckBoxSelected)
            cell.accessoryType = .none
        } else {
            cell.textLabel?.textColor = kColorGray
            cell.accessoryView = nil
            cell.accessoryType = .checkmark
        }
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
      
        if !selectedIndexes.contains(String(indexPath.row+1)) {
            selectedIndexes.append(String(indexPath.row+1))
            cell?.textLabel?.textColor = kColorRed
            cell?.accessoryView = UIImageView(image: imgCheckBoxSelected)
            cell?.accessoryType = .none
            
        } else {
            
            if selectedIndexes.contains(String(indexPath.row+1)) {
                
                let index = selectedIndexes.index(of: String(indexPath.row+1))
                if index != nil {
                    selectedIndexes.remove(at: index!)
                }
                
                cell?.textLabel?.textColor = kColorGray
                cell?.accessoryView = nil
                cell?.accessoryType = .checkmark
            }
        }
    }
}

extension SearchVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchVCCell")
        cell?.tintColor = kColorGray
        cell?.textLabel?.text = items[indexPath.row].localized()
        cell?.selectedBackgroundView = BlankView
        
        if (selectedIndexes.contains(String(indexPath.row+1))) {
            cell?.textLabel?.textColor = kColorRed
            cell?.accessoryView = UIImageView(image: imgCheckBoxSelected)
            cell?.accessoryType = .none
        } else {
            cell?.textLabel?.textColor = kColorGray
            cell?.accessoryView = nil
            cell?.accessoryType = .checkmark
        }
        return cell!
    }
}
