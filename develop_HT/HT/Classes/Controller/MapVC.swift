//
//  MapVC.swift
//  HT
//
//  Created by Dharmesh on 07/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import MapKit

protocol MapVCDelegate {
    
    func annotationDidChange(locationCoordinate2D : CLLocationCoordinate2D)
}

class MapVC : BaseVC {
    
    var delegate : MapVCDelegate?
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var btnUserThisLocation: UIButton!
    
    var productName : String!
    var locationCoordinate2D : CLLocationCoordinate2D!
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func addAnnotation(title : String, coordinate : CLLocationCoordinate2D) {
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = title
        self.mapView.addAnnotation(annotation)
        
        let miles : Double = 5.0;
        let scalingFactor : Double = abs(cos(2 * M_PI * coordinate.latitude / 360.0))
        
        var span : MKCoordinateSpan = MKCoordinateSpan()
        
        span.latitudeDelta = miles/69.0;
        span.longitudeDelta = miles/(scalingFactor * 69.0);
        
        var region : MKCoordinateRegion = MKCoordinateRegion()
        region.span = span;
        region.center = coordinate;
        
        self.mapView?.setRegion(region, animated: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
        if locationCoordinate2D == nil && touches.first != nil {
            
            let touchPoint = touches.first?.location(in: mapView)
            let touchMapCoordinate = mapView.convert(touchPoint!, toCoordinateFrom: mapView)
            locationCoordinate2D = touchMapCoordinate
            addAnnotation(title: "Current location".localized(), coordinate: locationCoordinate2D)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnUseThisLocationTapped(_ sender: UIButton) {
        
        if locationCoordinate2D != nil {
            delegate?.annotationDidChange(locationCoordinate2D: locationCoordinate2D)
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIView Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "USE THIS LOCATION".localized()
        mapView.delegate = self
        
        if (btnUserThisLocation != nil) {
            LocalizedControl(view: btnUserThisLocation, key : "USE THIS LOCATION")
        }
        
        if productName != nil && locationCoordinate2D != nil {
            addAnnotation(title: productName, coordinate: locationCoordinate2D!)
        } else if (locationCoordinate2D != nil) {
            addAnnotation(title: "Current location".localized(), coordinate: locationCoordinate2D!)
        }
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}

extension MapVC : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else { return nil }
        
        let identifier = "ProductDetailAnnotation"
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.animatesDrop = true
            annotationView?.isDraggable = true
            annotationView?.canShowCallout = false
            annotationView?.rightCalloutAccessoryView = UIButton(type: .infoLight)
        } else {
            annotationView?.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        switch newState {
        case .starting:
            view.dragState = .dragging
        case .ending, .canceling:
            view.dragState = .none
            locationCoordinate2D = mapView.annotations[0].coordinate
        default: break
        }
    }
}
