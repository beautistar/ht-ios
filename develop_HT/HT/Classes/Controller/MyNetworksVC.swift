//
//  MyNetworksVC.swift
//  HT
//
//  Created by Dharmesh on 02/12/16.
//  Copyright © 2016 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift

class MyNetworksVC : BaseVC, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollMyNetwork: UIScrollView!
    @IBOutlet weak var btnFollowing: UIButton!
    @IBOutlet weak var btnFollowers: UIButton!
    @IBOutlet weak var btnFindFriends: UIButton!
    @IBOutlet weak var layoutLineLeading: NSLayoutConstraint!
    
    var followingVC : FollowingVC!
    var followersVC : FollowersVC!
    var findFriendVC : FindFriendsVC!
    
    var controllers : [UIViewController]! {
        return [followingVC, followersVC, findFriendVC]
    }
    
    var isMenuScreen : Bool = true
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        MyNotificationCenter.removeObserver(self)        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func confifureUI() {
        
        self.title = "My Networks".localized()
        
        MyNotificationCenter.addObserver(self, selector: #selector(profileTapped), name: NSNotification.Name(rawValue: kNotificationMyNetworkVC), object: nil)
        
        scrollMyNetwork.delegate = self
        btnFollowing.isSelected = true
        
        LocalizedControl(view: btnFollowers, key : "FOLLOWERS")
        LocalizedControl(view: btnFollowing, key : "FOLLOWING")
        LocalizedControl(view: btnFindFriends, key : "FIND FRIENDS")
        
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnFollowingTapped(_ sender: UIButton) {
        
        scrollMyNetwork.contentOffset = CGPoint(x: 0, y: 0)
        followingVC.viewWillAppear(true)
    }
    
    @IBAction func btnFollowersTapped(_ sender: UIButton) {
        
        scrollMyNetwork.contentOffset = CGPoint(x: ScreenRect.width, y: 0)
        followersVC.viewWillAppear(true)
    }
    
    @IBAction func btnFindFriendsTapped(_ sender: UIButton) {
        scrollMyNetwork.contentOffset = CGPoint(x: (ScreenRect.width * 2), y: 0)
    }
    
    //------------------------------------------------------
    
    //MARK: UISCrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageWidth = scrollView.bounds.width
        let offset = scrollView.contentOffset.x
        let currentPage = offset/pageWidth
        
        layoutLineLeading.constant = (offset/CGFloat(controllers.count))
       
        if currentPage == 0 {
            
            btnFollowing.isSelected = true
            btnFollowers.isSelected = false
            btnFindFriends.isSelected = false
        }
        
        if currentPage == 1 {
            btnFollowing.isSelected = false
            btnFollowers.isSelected = true
            btnFindFriends.isSelected = false
        }
        
        if currentPage == 2 {
            btnFollowing.isSelected = false
            btnFollowers.isSelected = false
            btnFindFriends.isSelected = true
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if btnFollowing.isSelected {
            followingVC.viewWillAppear(true)
        }
        
        if btnFollowers.isSelected {
            followersVC.viewWillAppear(true)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Notification
    
    func profileTapped(notification : Notification) {
        
        let controller = NavigationManager.singleton.screen(screenType: NavigationScreenType.NavigationScreenTypeProfile) as! ProfileVC
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SegueFollowingVC" {
            followingVC = segue.destination as? FollowingVC
        } else if (segue.identifier == "SegueFollowersVC") {
            followersVC = segue.destination as? FollowersVC
        } else if (segue.identifier == "SegueFindFriendsVC") {
            findFriendVC = segue.destination as? FindFriendsVC
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MyNotificationCenter.addObserver(self, selector: #selector(confifureUI), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        confifureUI()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isMenuScreen {
            self.setNavigationBarItem()            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        scrollMyNetwork.layoutIfNeeded()
        let width = scrollMyNetwork.bounds.width
        let height = scrollMyNetwork.bounds.height
        scrollMyNetwork.contentSize = CGSize(width: width * CGFloat(controllers.count), height: height)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //------------------------------------------------------
}

