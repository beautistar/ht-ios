//
//  LeftViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit
import Localize_Swift
import Toucan
import AFNetworking

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: NSInteger)
}

class LeftViewController : BaseVC, LeftMenuProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnWelcome: UIButton!
    @IBOutlet weak var btnMyProfile: UIButton!
    @IBOutlet weak var lblListed: UILabel!
    @IBOutlet weak var lblListedCount: UILabel!
    @IBOutlet weak var lblLiked: UILabel!
    @IBOutlet weak var lblLikedCount: UILabel!
    @IBOutlet weak var lblBought: UILabel!
    @IBOutlet weak var lblBoughtCount: UILabel!
    
    @IBOutlet weak var btnListed: UIButton!
    
    var items = ["Home", "Post", "My Networks", "Help", "Invite Friends", "Settings", "Logout"]
    var icons = ["ic_menu_home", "ic_menu_camera", "ic_menu_people_know", "ic_menu_help", "ic_menu_share", "ic_menu_settings", "ic_menu_logout"]
    
    var currentNavigationVC: UINavigationController!
    
    var navigationMainVC: UINavigationController!
    var navigationPostVC: UINavigationController!
    var navigationMyNetworkVC : UINavigationController!
    var navigationHelpVC : UINavigationController!
    var navigationInviteFriendsVC : UINavigationController!
    var navigationSettingsVC : UINavigationController!
    
    //------------------------------------------------------
    
    //MARK: Memory Management Methods 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
         MyNotificationCenter.removeObserver(self)
    }
    
    //------------------------------------------------------
    
    //MARK: Custom Methods
    
    func configureUI() {
        
        LocalizedControl(view: lblListed, key: "Listed")
        LocalizedControl(view: lblLiked, key: "Liked")
        LocalizedControl(view: lblBought, key: "Bought")
        LocalizedControl(view: btnMyProfile, key: "My Profile")
        LocalizedControl(view: btnWelcome, key: "Welcome")
        LocalizedControl(view: btnListed, key: "Listed")
        
        tableView.reloadData()
    }
    
    //------------------------------------------------------
    
    //MARK: Action Methods
    
    @IBAction func btnProfileTapped(_ sender: UIButton) {
        
        self.slideMenuController()?.closeLeft()
        switch currentNavigationVC {
        case navigationMainVC:
            MyNotificationCenter.post(name:  NSNotification.Name(rawValue: kNotificationHomeVC), object: nil)
            break
        case navigationPostVC:
            MyNotificationCenter.post(name:  NSNotification.Name(rawValue: kNotificationPostVC), object: nil)
            break
        case navigationMyNetworkVC:
            MyNotificationCenter.post(name:  NSNotification.Name(rawValue: kNotificationMyNetworkVC), object: nil)
            break
        case navigationHelpVC:
            MyNotificationCenter.post(name:  NSNotification.Name(rawValue: kNotificationHelpVC), object: nil)
            break
        case navigationSettingsVC:
            MyNotificationCenter.post(name:  NSNotification.Name(rawValue: kNotificationSettingsVC), object: nil)
            break
        default:
            break
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         MyNotificationCenter.addObserver(self, selector: #selector(configureUI), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        configureUI()
        
        //TODO: showing the item you like!
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        
        navigationMainVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeHome)
        navigationPostVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypePost)
        navigationMyNetworkVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeMyNetwords)
        navigationHelpVC =  NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeHelp)
        navigationInviteFriendsVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeInviteFriends)
        navigationSettingsVC = NavigationManager.singleton.navigation(screenType: NavigationType.NavigationTypeSettings)
        
        /*let nonMenuController = storyboard.instantiateViewController(withIdentifier: "NonMenuController") as! NonMenuController
        nonMenuController.delegate = self
        self.nonMenuViewController = UINavigationController(rootViewController: nonMenuController)*/
        
        currentNavigationVC = navigationMainVC
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if userData != nil {
            
            //image
            if userData!.userimage != nil && URL(string: userData!.userimage) != nil {
                //imgUser.setImageWith(URL(string: userData!.userimage)!, placeholderImage: #imageLiteral(resourceName: "user_no_img"))
                
                let imgDownloadRequest = URLRequest(url: URL(string: userData!.userimage)!)
                imgUser.setImageWith(imgDownloadRequest, placeholderImage: #imageLiteral(resourceName: "user_no_img"), success: { (request : URLRequest, response : HTTPURLResponse?, image : UIImage) in
                    
                   self.imgUser.image = Toucan(image: image).maskWithEllipse().image
                    
                }, failure: { (request : URLRequest, response : HTTPURLResponse?, error : Error) in
                    
                })
            } else if (userData!.imgProfile != nil) {
                imgUser.image = userData!.imgProfile
            } else {
                imgUser.image = #imageLiteral(resourceName: "user_no_img")
            }
            
            //name
            if userData?.username != nil {
                btnMyProfile.setTitle(userData!.username, for: .normal)
            }
            
            if AFNetworkReachabilityManager.shared().isReachable {
                
                let request = "\(kAPIProfile)"
                
                let requestParam = RequestParamModal()
                requestParam.action = kAPIProfile
                requestParam.userId = String(userId)
                
                RequestManager.singleton.requestPOST(requestMethod: request, parameter: requestParam.toDictionary(), showLoader: false, successBlock: { (resposne :MessageModal, message : String?) in
                    
                    self.lblListedCount.text = String(resposne.userProfile.listOfProducts.count)
                    self.lblLikedCount.text = String(resposne.userProfile.productLikes.count)
                    self.lblBoughtCount.text = String(resposne.userProfile.productBought.count)
                    
                }, failureBlock: {(error : Error?) in
                    
                })
            }
            
        } else {
            imgUser.image = #imageLiteral(resourceName: "user_no_img")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func changeViewController(_ menu: NSInteger) {
        
        switch menu {
        case 0:
            currentNavigationVC = navigationMainVC
            self.slideMenuController()?.changeMainViewController(self.navigationMainVC, close: true)
        case 1:
            currentNavigationVC = navigationPostVC
            self.slideMenuController()?.changeMainViewController(self.navigationPostVC, close: true)
            break
        case 2:
            currentNavigationVC = navigationMyNetworkVC
            self.slideMenuController()?.changeMainViewController(self.navigationMyNetworkVC, close: true)
            break
        case 3:
            currentNavigationVC = navigationHelpVC
            self.slideMenuController()?.changeMainViewController(self.navigationHelpVC, close: true)
            break
        case 4:
            //currentNavigationVC = navigationInviteFriendsVC
            //self.slideMenuController()?.changeMainViewController(self.navigationInviteFriendsVC, close: true)
            NavigationManager.singleton.shareIt(view: self, message: "", image: nil)
            break
        case 5:
            currentNavigationVC = navigationSettingsVC
            self.slideMenuController()?.changeMainViewController(self.navigationSettingsVC, close: true)
            break
        case 6 :
            MyUserDefaults.removeObject(forKey: kCurrentUser);
            MyUserDefaults.set(Int("0"), forKey: kCurrentUserId)
            MyUserDefaults.synchronize()
            NavigationManager.singleton.setupLogin()
            break
        default :
            break
        }
    }
}

extension LeftViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.changeViewController(indexPath.row)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
        if self.tableView == scrollView {
            
        }
    }
}

extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftViewControllerCell")
        cell?.imageView?.image = UIImage(named: icons[indexPath.row])
        cell?.textLabel?.text = items[indexPath.row].localized()
        return cell!
    }
}
